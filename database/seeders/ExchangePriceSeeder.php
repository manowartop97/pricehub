<?php

namespace Database\Seeders;

use App\Models\Location\Location;
use App\Models\Location\Region;
use Illuminate\Database\Seeder;

class ExchangePriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Region::query()
            ->whereNotIn('name', ['Львівська', 'Івано-Франківська', 'Закарпатська'])
            ->update(['exchange_price' => env('OES_EXCHANGE_PRICE')]);

        Region::query()
            ->whereIn('name', ['Львівська', 'Івано-Франківська', 'Закарпатська'])
            ->update(['exchange_price' => env('BUOS_EXCHANGE_PRICE')]);
    }
}
