<?php

namespace Database\Seeders;

use App\Models\Provider\Provider;
use App\Models\Provider\ProviderHasLocation;
use App\Models\Location\Location;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * Class GasProvidersSeeder
 * @package Database\Seeders
 */
class ProvidersSeeder extends Seeder
{
    /**
     * @const
     */
    protected const DATA = [
        [
            'name'   => 'АТ "Вінницягаз"',
            'tariff' => null,
            'locations' => [
                [
                    ['where', ['region_name', '=', 'Вінницька']],
                    ['where_not_in', 'index', ['22021', '22141', '22221', '23214', '23222', "23223", '23652', "24028", "23609", "23636", "23650", "23644", "23660", "23623", "24215", "24216", "24124", "24133", "24123", "24126"]],
                ]
            ]
        ],
        [
            'name'   => 'АТ "Закарпаттягаз"',
            'tariff' => null,
            'locations' => [
                [
                    ['where', ['region_name', '=', 'Закарпатська']]
                ]
            ]
        ],
        [
            'name'   => 'АТ "Оператор Газорозподільної Системи "Луганськгаз""',
            'tariff' => null,
            'locations' => [
                [
                    ['where', ['region_name', '=', 'Луганська']]
                ]
            ]
        ],
        [
            'name'   => 'АТ "Київгаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where', ['region_name', '=', 'Київ']]
                ]
            ]
        ],
        [
            'name'   => 'АТ "Оператор газорозподільної системи Київоблгаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where', ['region_name', '=', 'Київська']]
                ]
            ]
        ],
        [
            'name'   => 'АТ "Оператор газорозподільної системи Чернівцігаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where', ['region_name', '=', 'Чернівецька']]
                ]
            ]
        ],
        [
            'name'   => 'ПАТ "Уманьгаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Черкаська']],
                    ['where_in', 'district_name', ['Умань', 'Уманський', 'Христинівський', 'Маньківський']]
                ]
            ]
        ],
        [
            'name'   => 'АТ "Одесагаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Одеська']]
                ]
            ]
        ],
        [
            'name'   => 'ВАТ "Кіровоградгаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'district_name', ['Кропивницький (місто)']]
                ],
                [
                    ['where_in', 'region_name', ['Кіровоградська']],
                ],
            ]
        ],
        [
            'name'   => 'АТ "Оператор газорозподільної системи "Дніпропетровськгаз""',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Дніпропетровська']],
                    ['where_not_in', 'district_name', ['Дніпро (місто)', 'Кривий Ріг', 'Дніпровський', 'Криворізький', 'Дніпропетровськ']],
                    ['where_not_in', 'index', ['52001', '52002', '52005', '52063', '52060', '52061', '52069', '52070', '52070', '52064', '52026', '52053', '52052', '52035', '52041', '52030', '52073', '52043', '52074', '52075', '52072', '52048']],
                ]
            ]
        ],
        [
            'name'   => 'АТ "Оператор газорозподільної системи "Дніпрогаз""',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'district_name', ['Дніпро (місто)']],
                ],
                [
                    ['where_in', 'district_name', ['Дніпровський']],
                    ['where_in', 'index', ['52001', '52002', '52005', '52063', '52060', '52061', '52069', '52070', '52070', '52064', '52026', '52053', '52052', '52035', '52041', '52030', '52073', '52043', '52074', '52075', '52072', '52048']],
                ],
            ]
        ],
        [
            'name'   => 'ПАТ "Маріупольгаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where', ['region_name', '=', 'Донецька']],
                    ['where_in', 'district_name', ['Мангушський', 'Нікольський', 'Новоазовський', 'Маріуполь']]
                ],
                [
                    ['where_in', 'index', ['87640', '84583', '87641', '87663', '87645']]
                ],
            ]
        ],
        [
            'name'   => 'ПАТ По Газопостачанню Та Газифікації "Донецькоблгаз"',
            'tariff' => null,
            'locations' => [
                [
                    ['where', ['region_name', '=', 'Донецька']],
                    ['where_not_in', 'district_name', ['Мангушський', 'Нікольський', 'Новоазовський', 'Маріуполь']],
                    ['where_not_in', 'index', ['87640', '84583', '87641']]
                ],
            ]
        ],
        [
            'name'   => 'АТ "Черкасигаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Черкаська']],
                    ['where_not_in', 'district_name', ['Умань', 'Уманський', 'Христинівський', 'Маньківський']]
                ]
            ]
        ],
        [
            'name'   => 'АТ "Харківгаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Харківська']],
                    ['where_not_in', 'district_name', ['Харків (місто)']],
                    ['where_not_in', 'index', ['62066']],
                ]
            ]
        ],
        [
            'name'   => 'АТ "Харківміськгаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'district_name', ['Харків (місто)']],
                ],
                [
                    ['where_in', 'index', ['62448', '62449', '62450', '62447', '62921', '62446']]
                ]
            ]
        ],
        [
            'name'   => 'АТ "Рівнегаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Рівненська']],
                ]
            ]
        ],
        [
            'name'   => 'АТ "Хмельницькгаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Хмельницька']],
                    ['where_not_in', 'district_name', ['Шепетівський', 'Шепетівка']],
                ]
            ]
        ],
        [
            'name'   => 'ПАТ "Шепетівкагаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'district_name', ['Шепетівський', 'Шепетівка']],
                ]
            ]
        ],
        [
            'name'   => 'АТ "Тисменицягаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Івано-Франківська']],
                    ['where_in', 'district_name', ['Тисменицький']],
                    ['where_not_in', 'index', ['77460', '77458', '77451', '77450', '77423', '77421', '77422', '77442', '77424']],
                ]
            ]
        ],
        [
            'name'   => 'АТ "Запоріжжягаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Запорізька']],
                    ['where_not_in', 'district_name', ['Мелітополь', 'Мелітопольський', 'Веселівський', 'Приазовський']],
                    ['where_not_in', 'index', ['70210']],
                ]
            ]
        ],
        [
            'name'   => 'АТ "Волиньгаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Волинська']],
                ]
            ]
        ],
        [
            'name'   => 'АТ "Мелітопольгаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Запорізька']],
                    ['where_in', 'district_name', ['Мелітополь', 'Мелітопольський', 'Веселівський', 'Приазовський']],
                ]
            ]
        ],
        [
            'name'   => 'АТ "Миколаївгаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Миколаївська']],
                ]
            ]
        ],
        [
            'name'   => 'АТ "Житомиргаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Житомирська']],
                    ['where_in', 'district_name', ['Бердичівський', 'Коростенський', 'Новоград-Волинський', 'Житомир (місто)']],
                ],
                [
                    ['where_in', 'index', ['12201', '13201', "12601", "12265", "13514", "13515", "13101", "13033", "13551", "13501", "12001", "13001", "12101", "12301", "13543", "12411", "12214", "13224", "13701", "12446", "12403", "12030", "12031", "12402", "12225", "12430", "12420", "22221", "22220", "22141"]],
                ]
            ]
        ],
        [
            'name'   => 'АТ "Львівгаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Львівська']],
                    ['where_not_in', 'index', ['81652', '81653', "81650"]],
                ]
            ]
        ],
        [
            'name'   => 'ПАТ "Тернопільміськгаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'district_name', ['Тернопіль (місто)', 'Тернопільський']],
                    ['where_not_in', 'index', ['47708', "47752", "47723", "47726", "47721"]],
                ]
            ]
        ],
        [
            'name'   => 'АТ "Сумигаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Сумська']],
                ],
                [
                    ['where_in', 'index', ['62066']],
                ],
            ]
        ],
        [
            'name'   => 'АТ "Чернігівгаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Чернігівська']],
                ],
            ]
        ],
        [
            'name'   => 'АТ "Івано-Франківськгаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Івано-Франківська']],
                    ['where_not_in', 'district_name', ['Тисменицький']],
                    ['where_not_in', 'index', ['77401', "77460", "77458", "77451", "77450", "77423", "77421", "77422", "77442", "77424"]]
                ],
                [
                    ['where_in', 'index', ['77460', '77458', '77451', '77450', '77423', '77421', '77422', '77442', '77424']]
                ]
            ]
        ],
        [
            'name'   => 'ПАТ "Коростишівгаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'district_name', ['Коростишівський']],
                ],
            ]
        ],
        [
            'name'   => 'ПАТ "Гадячгаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Полтавська']],
                    ['where_in', 'district_name', ['Гадяцький', 'Гадяч']],
                ],
            ]
        ],
        [
            'name'   => 'АТ "Полтавагаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Полтавська']],
                    ['where_not_in', 'district_name', ['Гадяцький', 'Кременчук', 'Лубни', 'Лубенський', 'Горішні Плавні', 'Кременчуцький', 'Пирятинський', "Оржицький", "Гребінківський", "Семенівський", "Кременчук", 'Гадяч']],
                ],
            ]
        ],
        [
            'name'   => 'ПАТ "Кременчукгаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Полтавська']],
                    ['where_in', 'district_name', ['Кременчук', "Горішні Плавні", "Семенівський", 'Кременчуцький']],
                ],
            ]
        ],
        [
            'name'   => 'АТ "Лубнигаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Полтавська']],
                    ['where_in', 'district_name', ['Лубни', "Гребінківський", "Пирятинський", "Оржицький", 'Лубенський']],
                ],
            ]
        ],
        [
            'name'   => 'АТ "Херсонгаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Херсонська']],
                ],
            ]
        ],
        [
            'name'   => 'АТ "Криворіжгаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'district_name', ['Кривий Ріг', 'Криворізький']],
                ],
            ]
        ],
        [
            'name'   => 'ПАТ "Тернопільгаз"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Тернопільська']],
                    ['where_not_in', 'district_name', ['Тернопіль (місто)', 'Тернопільський']],
                    ['where_not_in', 'index', ['47002', '47003', "47004", "47024", "47013", "47026", "47058", "47018", "47071", "47042", "47035", "47023", "47030", "47016", "47021", "47075", "47050", "47054", "47052", "47014", "47017", "47076", "47039", "47078", "47040", "47034", "47045", "47060", "47055", "47031", "47046", "47012", "47047", "47079", "47070", "47032"]],
                ],
                [
                    ['where_in', 'index', ['47708', "47752", "47723", "47726", "47721"]]
                ]
            ]
        ],
        [
            'name'   => 'ПАТ "Новороздільське Гірничо-Хімічне Підприємство "Сірка""',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'index', ['81652', '81653', "81650"]],
                ],
            ]
        ],
        [
            'name'   => 'ТОВ "Газпостачсервіс"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'index', ['23214', '23222', "23223"]],
                ],
            ]
        ],
        [
            'name'   => 'ТОВ "Газовик"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'index', ['23652', "24028", "23609", "23636", "23650", "23644", "23660", "23623", "24215", "24216", "24124", "24133", "24123", "24126"]],
                ],
            ]
        ],
        [
            'name'   => 'ДП "Кременецьке Управління з Постачання та Реалізації Газу"',
            'tariff' => null,

            'locations' => [
                [
                    ['where_in', 'index', ['47002', '47003', "47004", "47024", "47013", "47026", "47058", "47018", "47071", "47042", "47035", "47023", "47030", "47016", "47021", "47075", "47050", "47054", "47052", "47014", "47017", "47076", "47039", "47078", "47040", "47034", "47045", "47060", "47055", "47031", "47046", "47012", "47047", "47079", "47070", "47032"]],
                ],
            ]
        ],
    ];

    /**
     *
     */
    protected const ENERGY_DATA = [
        [
            'name'   => 'ПАТ "Запоріжжяобленерго"',
            'tariff' => 843.90,

            'locations' => [
                [
                    ['where', ['region_name', '=', 'Запорізька']]
                ]
            ]
        ],
        [
            'name'   => 'ВАТ "Тернопільобленерго"',
            'tariff' => 1211.75,

            'locations' => [
                [
                    ['where', ['region_name', '=', 'Тернопільська']],
                ]
            ]
        ],
        [
            'name'   => 'АТ "ДТЕК Одеські Електромережі"',
            'tariff' => 890.28,

            'locations' => [
                [
                    ['where', ['region_name', '=', 'Одеська']]
                ]
            ]
        ],
        [
            'name'   => 'ТОВ "Луганське Енергетичне Об\'єднання"',
            'tariff' => 820.39,

            'locations' => [
                [
                    ['where', ['region_name', '=', 'Луганська']]
                ]
            ]
        ],
        [
            'name'   => 'АТ "Харківобленерго"',
            'tariff' => 820.39,

            'locations' => [
                [
                    ['where', ['region_name', '=', 'Харківська']]
                ]
            ]
        ],
        [
            'name'      => 'АТ "Вінницяобленерго"',
            'tariff'    => 1150.40,
            'locations' => [
                [
                    ['where_in', 'region_name', ['Вінницька']],
                ],
            ]
        ],
        [
            'name'   => 'АТ Чернівціобленерго',
            'tariff' => 991.84,

            'locations' => [
                [
                    ['where', ['region_name', '=', 'Чернівецька']],
                ],
            ]
        ],
        [
            'name'   => 'АТ Хмельницькобленерго',
            'tariff' => 1121.43,

            'locations' => [
                [
                    ['where', ['region_name', '=', 'Хмельницька']],
                ],
            ]
        ],
        [
            'name'   => 'ПАТ Кіровоградобленерго',
            'tariff' => 1179.59,

            'locations' => [
                [
                    ['where', ['region_name', '=', 'Кіровоградська']],
                ],
            ]
        ],
        [
            'name'   => 'ПАТ Волиньобленерго',
            'tariff' => 1020.03,

            'locations' => [
                [
                    ['where', ['region_name', '=', 'Волинська']],
                    ['where_not_in', 'district_name', ['Нововолинськ', 'Іваничівський']]
                ],
            ]
        ],
        [
            'name'   => 'АТ Житомиробленерго',
            'tariff' => 1188.01,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Житомирська']],
                ],
            ]
        ],
        [
            'name'   => 'АТ Херсонобленерго',
            'tariff' => 1028.86,

            'locations' => [
                [
                    ['where', ['region_name', '=', 'Херсонська']],
                ],
            ]
        ],
        [
            'name'   => 'ПАТ Закарпаттяобленерго',
            'tariff' => 1266.75,

            'locations' => [
                [
                    ['where', ['region_name', '=', 'Закарпатська']],
                ],
            ]
        ],
        [
            'name'   => 'ПАТ Рівнеобленерго',
            'tariff' => 926.21,

            'locations' => [
                [
                    ['where', ['region_name', '=', 'Рівненська']],
                    ['where_not_in', 'district_name', ['Вараш']],
                ],
            ]
        ],
        [
            'name'   => 'АТ Миколаївобленерго',
            'tariff' => 987.35,

            'locations' => [
                [
                    ['where', ['region_name', '=', 'Миколаївська']],
                    ['where_not_in', 'index', ['55001']],
                ],
            ]
        ],
        [
            'name'   => 'ПАТ Атомсервіс',
            'tariff' => 1137.63,

            'locations' => [
                [
                    ['where_in', 'index', ['55001']],
                ],
            ]
        ],
        [
            'name'   => 'ПАТ Прикарпаттяобленерго',
            'tariff' => 1276.22,

            'locations' => [
                [
                    ['where', ['region_name', '=', 'Івано-Франківська']],
                ],
            ]
        ],
        [
            'name'   => 'ПАТ Львівобленерго',
            'tariff' => 955.36,

            'locations' => [
                [
                    ['where', ['region_name', '=', 'Львівська']],
                    ['where_not_in', 'district_name', ['Червоноград', 'Сокальський']],
                    ['where_not_in', 'index', ['81053', '81652', '81653']],
                ],
            ]
        ],
        [
            'name'   => 'ПАТ Чернігівобленерго',
            'tariff' => 1188.69,

            'locations' => [
                [
                    ['where', ['region_name', '=', 'Чернігівська']],
                ],
                [
                    ['where', ['index', '=', '07101']],
                ],
            ]
        ],
        [
            'name'   => 'ПРАТ ДТЕК Київські Електромережі',
            'tariff' => 374.28,

            'locations' => [
                [
                    ['where_in', 'district_name', ['Київ (місто)']],
                ],
            ]
        ],
        [
            'name'   => 'ПРАТ ДТЕК Київські Регіональні Електромережі',
            'tariff' => 1508.28,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Київська']],
                    ['where_not_in', 'district_name', ['Київ (місто)']],
                    ['where_not_in', 'index', ['07101']],
                ],
            ]
        ],
        [
            'name'   => 'ПАТ Черкасиобленерго',
            'tariff' => 928.33,

            'locations' => [
                [
                    ['where', ['region_name', '=', 'Черкаська']],
                ],
            ]
        ],
        [
            'name'   => 'АТ Полтаваобленерго',
            'tariff' => 932.73,

            'locations' => [
                [
                    ['where', ['region_name', '=', 'Полтавська']],
                ],
            ]
        ],
        [
            'name'   => 'АТ Сумиобленерго',
            'tariff' => 1226.67,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Сумська']],
                ]
            ]
        ],
        [
            'name'   => 'АТ ДТЕК Дніпровські Електромережі',
            'tariff' => 697.92,

            'locations' => [
                [
                    ['where_in', 'region_name', ['Дніпропетровська']],
                ]
            ]
        ],
//        [
//            'name'   => 'ПРАТ "ПЕЕМ "ЦЕК"',
//            'tariff' => 759.88,
//
//            'locations' => [
//                [
//                    ['where_in', 'district_name', ['Дніпро (місто)']],
//                ]
//            ]
//        ],
        [
            'name'   => 'АТ ДТЕК Донецькі Електромережі',
            'tariff' => 1121.27,

            'locations' => [
                [
                    ['where', ['region_name', '=', 'Донецька']],
                    ['where_not_in', 'index', ['87501']],
                ],
            ]
        ],
//        [
//            'name'   => 'ПРАТ ДТЕК ПЕМ-Енерговугілля',
//            'tariff' => 245.09,
//
//            'locations' => [
//                [
//                    ['where_in', 'index', ['85670', '87501', '85200']],
//                ],
//            ]
//        ],
        [
            'name'   => 'ТОВ Нафтогаз Тепло',
            'tariff' => 775.66,

            'locations' => [
                [
                    ['where_in', 'index', ['81053']],
                ],
            ]
        ],
        [
            'name'   => 'ТОВ Нафтогаз Тепло (Новий Розділ)',
            'tariff' => 1300.39,

            'locations' => [
                [
                    ['where_in', 'index', ['81652', '81653']],
                ],
            ]
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('provider_has_locations')->truncate();
        DB::table('providers')->truncate();

        foreach (self::DATA as $providerData) {

            $provider = Provider::query()->create([
                'type'   => Provider::TYPE_GAS,
                'name'   => $providerData['name'],
                'tariff' => $providerData['tariff'],

            ]);

            $this->processLocationsData($providerData['locations'], $provider);
        }

        foreach (self::ENERGY_DATA as $providerData) {

            $provider = Provider::query()->create([
                'type'   => Provider::TYPE_ENERGY,
                'name'   => $providerData['name'],
                'tariff' => $providerData['tariff'],

            ]);

            $this->processLocationsData($providerData['locations'], $provider);
        }
        Schema::enableForeignKeyConstraints();
    }

    /**
     * @param array $locationData
     * @param Model|Provider $provider
     */
    protected function processLocationsData(array $locationData, Model $provider)
    {
        $query = Location::query();
        foreach ($locationData as $id => $whereClause) {

            if ($id === 0) {
                $query->where(function (Builder $query) use ($provider, $whereClause) {
                    foreach ($whereClause as $queryParams) {
                        $query
                            ->when(($queryParams[0] ?? null) === 'where', function (Builder $query) use ($queryParams) {
                                $query->where($queryParams[1][0], $queryParams[1][1], $queryParams[1][2]);
                            })
                            ->when(($queryParams[0] ?? null) === 'where_in', function (Builder $query) use ($queryParams) {
                                $query->whereIn($queryParams[1], $queryParams[2]);
                            })
                            ->when(($queryParams[0] ?? null) === 'where_not_in', function (Builder $query) use ($queryParams) {
                                $query->whereNotIn($queryParams[1], $queryParams[2]);
                            });
                    }
                });

                continue;
            }

            $query->orWhere(function (Builder $query) use ($provider, $whereClause) {
                foreach ($whereClause as $queryParams) {
                    $query
                        ->when(($queryParams[0] ?? null) === 'where', function (Builder $query) use ($queryParams) {
                            $query->where($queryParams[1][0], $queryParams[1][1], $queryParams[1][2]);
                        })
                        ->when(($queryParams[0] ?? null) === 'where_in', function (Builder $query) use ($queryParams) {
                            $query->whereIn($queryParams[1], $queryParams[2]);
                        })
                        ->when(($queryParams[0] ?? null) === 'where_not_in', function (Builder $query) use ($queryParams) {
                            $query->whereNotIn($queryParams[1], $queryParams[2]);
                        });
                }
            });
        }

        $data = [];

        $query->get()->each(function (Location $location) use (&$data, $provider) {
            $data[$location->index] = [
                'provider_id' => $provider->getKey(),
                'location_id' => $location->getKey()
            ];
        });

        foreach (array_chunk($data, 500) as $chunk) {
            ProviderHasLocation::query()->insert($chunk);
        }
    }
}
