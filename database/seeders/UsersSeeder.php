<?php

namespace Database\Seeders;

use App\Models\User\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::query()->create([
            'name'     => 'Admin',
            'email'    => 'admin@admin.admin',
            'password' => Hash::make('123456'),
            'role'     => User::ROLE_ADMIN
        ]);

        User::query()->create([
            'name'     => 'User',
            'email'    => 'user@user.user',
            'password' => Hash::make('123456'),
            'role'     => User::ROLE_USER
        ]);
    }
}
