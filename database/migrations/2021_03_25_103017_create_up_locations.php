<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpLocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('up_regions', function (Blueprint $table) {
           $table->id();
           $table->string('name');
           $table->timestamps();
        });

        Schema::create('up_districts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('region_id');
            $table->string('name');
            $table->string('region_name');
            $table->timestamps();
            $table->foreign('region_id')->on('up_regions')->references('id')->onDelete('cascade');
        });

        Schema::create('up_cities', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('region_id');
            $table->unsignedBigInteger('district_id');
            $table->string('name');
            $table->string('type')->nullable();
            $table->string('region_name');
            $table->string('district_name');
            $table->timestamps();
            $table->foreign('region_id')->on('up_regions')->references('id')->onDelete('cascade');
            $table->foreign('district_id')->on('up_districts')->references('id')->onDelete('cascade');
        });

        Schema::create('up_streets', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('region_id');
            $table->unsignedBigInteger('city_id');
            $table->unsignedBigInteger('district_id');
            $table->string('name');
            $table->string('type')->nullable();
            $table->string('region_name');
            $table->string('district_name');
            $table->string('city_name');
            $table->timestamps();
            $table->foreign('region_id')->on('up_regions')->references('id')->onDelete('cascade');
            $table->foreign('district_id')->on('up_districts')->references('id')->onDelete('cascade');
            $table->foreign('city_id')->on('up_cities')->references('id')->onDelete('cascade');
        });

        Schema::create('up_houses', function (Blueprint $table) {
            $table->id();
            $table->string('index', 8);
            $table->unsignedBigInteger('street_id');
            $table->string('number');
            $table->timestamps();
            $table->foreign('street_id')->on('up_streets')->references('id')->onDelete('cascade');
        });

        Schema::create('up_locations', function (Blueprint $table) {
            $table->id();
            $table->string('region_name');
            $table->string('district_name');
            $table->string('name')->index();
            $table->string('index', 8)->index();
            $table->float('osr_price');
            $table->float('osp_price');
            $table->float('exchange_price');
            $table->string('location_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('up_locations');
        Schema::dropIfExists('up_houses');
        Schema::dropIfExists('up_streets');
        Schema::dropIfExists('up_cities');
        Schema::dropIfExists('up_districts');
        Schema::dropIfExists('up_regions');
    }
}
