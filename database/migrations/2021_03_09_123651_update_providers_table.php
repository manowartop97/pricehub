<?php

use App\Models\Provider\Provider;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UpdateProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('gas_provider_has_locations');
        Schema::dropIfExists('gas_providers');
        Schema::dropIfExists('provider_has_locations');
        Schema::dropIfExists('providers');

        Schema::create('providers', function (Blueprint $table) {
            $table->id();
            $table->integer('type')->default(Provider::TYPE_GAS);
            $table->string('name');
            $table->float('tariff');
            $table->integer('tariff_type');
            $table->text('information')->nullable();
            $table->timestamps();
        });

        Schema::create('provider_has_locations', function (Blueprint $table) {
            $table->bigInteger('provider_id');
            $table->bigInteger('location_id');
            $table->primary(['provider_id', 'location_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provider_has_locations');
        Schema::dropIfExists('providers');
    }
}
