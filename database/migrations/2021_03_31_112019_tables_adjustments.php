<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablesAdjustments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('suppliers_has_locations');

        Schema::dropIfExists('locations');

        Schema::table('up_regions', function (Blueprint $table) {
            if (!Schema::hasColumn('up_regions', 'exchange_price')) {
                $table->float('exchange_price')->default(35);
            }
        });

        Schema::table('up_locations', function (Blueprint $table) {
            if (Schema::hasColumn('up_locations', 'osr_price')) {
                $table->dropColumn('osr_price');
            }

            if (Schema::hasColumn('up_locations', 'osp_price')) {
                $table->dropColumn('osp_price');
            }

            if (Schema::hasColumn('up_locations', 'exchange_price')) {
                $table->dropColumn('exchange_price');
            }
        });

        Schema::table('providers', function (Blueprint $table) {
            if (!Schema::hasColumn('providers', 'osr_price')) {
                $table->float('osr_price')->nullable()->default(5.12);
            }
        });
    }

    /**
     * No need to revert
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
