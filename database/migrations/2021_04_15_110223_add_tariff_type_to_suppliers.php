<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTariffTypeToSuppliers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gas_business_suppliers', function (Blueprint $table) {
            $table->integer('tariff_type')->default(1);
        });

        Schema::table('energy_business_suppliers', function (Blueprint $table) {
            $table->integer('tariff_type')->default(1);
        });

        Schema::table('gas_client_suppliers', function (Blueprint $table) {
            $table->integer('tariff_type')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gas_business_suppliers', function (Blueprint $table) {
            $table->dropColumn('tariff_type');
        });

        Schema::table('energy_business_suppliers', function (Blueprint $table) {
            $table->dropColumn('tariff_type');
        });

        Schema::table('gas_client_suppliers', function (Blueprint $table) {
            $table->dropColumn('tariff_type');
        });
    }
}
