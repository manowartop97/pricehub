<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->id();
            $table->integer('index')->index();
            $table->string('name');
        });

        Schema::create('gas_business_suppliers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('logo')->nullable();
            $table->string('formula');
            $table->text('features')->nullable();
            $table->timestamps();
        });

        Schema::create('energy_business_suppliers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('logo')->nullable();
            $table->string('formula');
            $table->text('features')->nullable();
            $table->timestamps();
        });

        Schema::create('gas_client_suppliers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('logo')->nullable();
            $table->string('formula');
            $table->text('features')->nullable();
            $table->timestamps();
        });

        Schema::create('suppliers_has_locations', function (Blueprint $table) {
            $table->id();
            $table->morphs('supplier');
            $table->unsignedBigInteger('location_id');
            $table->foreign('location_id')->on('locations')->references('id')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers_has_locations');
        Schema::dropIfExists('energy_business_suppliers');
        Schema::dropIfExists('gas_client_suppliers');
        Schema::dropIfExists('gas_business_suppliers');
        Schema::dropIfExists('locations');
    }
}
