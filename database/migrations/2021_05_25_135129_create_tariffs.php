<?php

use App\Services\Supplier\Formula\DefaultGas\DefaultGasFormula;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTariffs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tariffs')) {
            Schema::create('tariffs', function (Blueprint $table) {
                $table->id();
                $table->string('name');
                $table->float('price');
                $table->string('formula')->default('App\\Services\\Supplier\\Formula\\DefaultGas\\DefaultGasFormula');
                $table->morphs('model');
                $table->timestamps();
            });
        }

        Schema::table('energy_business_suppliers', function (Blueprint $table) {
            $table->dropColumn('formula');
        });

        Schema::table('gas_client_suppliers', function (Blueprint $table) {
            $table->dropColumn('formula');

            if (Schema::hasColumn('gas_client_suppliers', 'tariff_type')) {
                $table->dropColumn('tariff_type');
            }

            if (Schema::hasColumn('gas_client_suppliers', 'margin')) {
                $table->dropColumn('margin');
            }
        });

        Schema::table('gas_business_suppliers', function (Blueprint $table) {
            $table->dropColumn('formula');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tariffs');

        Schema::table('energy_business_suppliers', function (Blueprint $table) {
            $table->string('formula')->nullable();
        });

        Schema::table('gas_client_suppliers', function (Blueprint $table) {
            $table->string('formula')->nullable();
        });

        Schema::table('gas_business_suppliers', function (Blueprint $table) {
            $table->string('formula')->nullable();
        });
    }
}
