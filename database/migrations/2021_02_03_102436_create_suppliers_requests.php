<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliersRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_requests', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->longText('data');
            $table->timestamps();
        });

        Schema::create('supplier_fields', function (Blueprint $table) {
            $table->id();
            $table->morphs('model');
            $table->string('name');
            $table->string('validation');
            $table->boolean('is_required');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_requests');
        Schema::dropIfExists('supplier_fields');
    }
}
