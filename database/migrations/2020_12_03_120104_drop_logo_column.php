<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropLogoColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gas_business_suppliers', function (Blueprint $table) {
            $table->dropColumn('logo');
        });

        Schema::table('energy_business_suppliers', function (Blueprint $table) {
            $table->dropColumn('logo');
        });

        Schema::table('gas_client_suppliers', function (Blueprint $table) {
            $table->dropColumn('logo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gas_business_suppliers', function (Blueprint $table) {
            $table->string('logo')->nullable();
        });

        Schema::table('energy_business_suppliers', function (Blueprint $table) {
            $table->string('logo')->nullable();
        });

        Schema::table('gas_client_suppliers', function (Blueprint $table) {
            $table->string('logo')->nullable();
        });
    }
}
