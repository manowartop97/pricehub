<?php

use App\Http\Controllers\Api\V1\Provider\ProviderController;
use App\Http\Controllers\Api\V1\Location\LocationController;
use App\Http\Controllers\Api\V1\Supplier\SupplierController;
use App\Http\Controllers\Api\V1\SupplierRequest\SupplierRequestController;
use App\Http\Controllers\Api\V1\User\UserController;
use Illuminate\Support\Facades\Route;


/**
 * Api v1
 */
Route::prefix('v1')->group(function () {

    Route::get('suppliers/{type}/fields', [SupplierController::class, 'getFields']);

    /**
     * Auth routes
     */
    Route::group(['middleware' => ['auth:api']], function () {

        /**
         * User routes
         */
        Route::group(['prefix' => 'users', 'namespace' => 'User'], function () {
            Route::get('/me', [UserController::class, 'me']);
        });

        /**
         * Locations
         */
        Route::resource('locations', LocationController::class)
            ->except('index')
            ->middleware('admin');

        Route::get('suppliers/{type}/requests', [SupplierController::class, 'getRequests']);

        /**
         * Suppliers
         */
        Route::group(['prefix' => 'suppliers/{supplierService}', 'middleware' => ['admin']], function () {
            Route::get('', [SupplierController::class, 'index']);
            Route::get('{supplierModel}', [SupplierController::class, 'show']);
            Route::post('', [SupplierController::class, 'store']);
            Route::put('{supplierModel}', [SupplierController::class, 'update']);
            Route::post('{supplierModel}/logos/upload', [SupplierController::class, 'updateLogo']);
            Route::delete('{supplierModel}', [SupplierController::class, 'destroy']);
        });

        Route::get('providers/{type}', [ProviderController::class, 'index']);

        Route::resource('providers', ProviderController::class)
            ->except('index')
            ->middleware('admin');
    });

    Route::get('locations', [LocationController::class, 'index']);

    Route::post('supplier-requests', [SupplierRequestController::class, 'store']);
    Route::get('supplier-requests/additional-data', [SupplierRequestController::class, 'getAdditionalData']);
    Route::post('supplier-requests/files/upload', [SupplierRequestController::class, 'uploadFile']);

    Route::group(['prefix' => 'calculations'], function () {
        /**
         * Suppliers public routes
         */
        Route::group(['prefix' => 'suppliers/{supplierService}'], function () {
            Route::get('', [SupplierController::class, 'calculate']);
        });
    });
});
