<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Kolirt\Ukrposhta\Ukrposhta;
use Throwable;

/**
 * Class UkrPoshtaLocationsParserCommand
 * @package App\Console\Commands
 */
class UkrPoshtaLocationsParserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'up-locations:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws Exception
     */
    public function handle()
    {
        $locationsData = [];

        $regionsData = [];
        $districtsData = [];
        $citiesData = [];
        $streetsData = [];
        $housesData = [];

        $api = new Ukrposhta();

        DB::table('up_locations')->truncate();
        DB::table('up_houses')->truncate();
        DB::table('up_streets')->truncate();
        DB::table('up_cities')->truncate();
        DB::table('up_districts')->truncate();
        DB::table('up_regions')->truncate();

        $parsedCities = DB::table('up_cities')->pluck('id')->toArray();
        $parsedRegions = DB::table('up_regions')->pluck('id')->toArray();
        $parsedDistricts = DB::table('up_districts')->pluck('id')->toArray();
        $parsedStreets = DB::table('up_streets')->where('region_id', 26)->pluck('id')->toArray();

        try {
            $rData = $api->getRegions();
        } catch (Throwable $exception) {
            $rData = [];
        }

        foreach ($rData as $region) {

            if (in_array((int)$region->REGION_ID, $parsedRegions)) {
                $this->warn('Skipping region ' . $region->REGION_ID);
                continue;
            }

            $regionsData[] = [
                'id'   => $region->REGION_ID,
                'name' => $region->REGION_UA
            ];

            $this->insertData('up_regions', $regionsData);
            try {
                $dData = $api->getDistricts(null, $region->REGION_ID);
            } catch (Throwable $exception) {
                $dData = [];
            }

            foreach ($dData as $district) {

                if (in_array((int)$district->DISTRICT_ID, $parsedDistricts)) {
                    $this->warn('Skipping district ');
                    continue;
                }

                $districtsData[] = [
                    'id'          => $district->DISTRICT_ID,
                    'region_id'   => $region->REGION_ID,
                    'region_name' => $region->REGION_UA,
                    'name'        => $district->DISTRICT_UA
                ];

                $this->insertData('up_districts', $districtsData);
                try {
                    $cData = $api->getCities(null, $district->DISTRICT_ID);
                } catch (Throwable $exception) {
                    $cData = [];
                }

                foreach ($cData as $city) {

                    if (in_array((int)$city->CITY_ID, $parsedCities)) {
                        $this->warn('Skipping city ');
                        continue;
                    }

                    $citiesData[] = [
                        'id'            => $city->CITY_ID,
                        'type'          => $city->CITYTYPE_UA,
                        'name'          => $city->CITY_UA,
                        'region_id'     => $region->REGION_ID,
                        'region_name'   => $region->REGION_UA,
                        'district_id'   => $district->DISTRICT_ID,
                        'district_name' => $district->DISTRICT_UA
                    ];

                    $this->insertData('up_cities', $citiesData);
                    try {
                        $sData = $api->getStreets(null, $city->CITY_ID);
                    } catch (Throwable $exception) {
                        $sData = [];
                    }

                    foreach ($sData as $street) {

                        if (in_array((int)$street->STREET_ID, $parsedStreets)) {
                            $this->warn('Skipping street ');
                            continue;
                        }


                        $streetsData[] = [
                            'id'            => $street->STREET_ID,
                            'type'          => $street->STREETTYPE_UA,
                            'name'          => $street->STREET_UA,
                            'city_id'       => $city->CITY_ID,
                            'city_name'     => $city->CITY_UA,
                            'region_id'     => $region->REGION_ID,
                            'region_name'   => $region->REGION_UA,
                            'district_id'   => $district->DISTRICT_ID,
                            'district_name' => $district->DISTRICT_UA
                        ];
                        $this->insertData('up_streets', $streetsData);

                        try {
                            $hData = $api->getHouses($street->STREET_ID);
                        } catch (Throwable $exception) {
                            $hData = [];
                        }

                        foreach ($hData as $house) {

                            $housesData[] = [
                                'street_id' => $house->STREET_ID,
                                'index'     => $house->POSTCODE,
                                'number'    => $house->HOUSENUMBER_UA
                            ];

//                            $locationsData[$house->POSTCODE] = [
//                                'region_name'    => $region->REGION_UA,
//                                'district_name'  => $district->DISTRICT_UA,
//                                'name'           => $city->CITY_UA,
//                                'index'          => $house->POSTCODE,
//                                'osr_price'      => rand(1, 999),
//                                'osp_price'      => rand(1, 999),
//                                'exchange_price' => rand(1, 999),
//                                'location_type'  => $city->CITYTYPE_UA,
//                            ];
                        }
                        $this->insertData('up_houses', $housesData);
                    }
                }
            }
        }
    }

    /**
     * @param string $tableName
     * @param array $data
     */
    protected function insertData(string $tableName, array &$data = []): void
    {
        foreach (array_chunk($data, 3000) as $chunk) {
            $this->info('Inserting chunk into the ' . $tableName);
            DB::table($tableName)->insert($chunk);
        }

        $data = [];
    }
}
