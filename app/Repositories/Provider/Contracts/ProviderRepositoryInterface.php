<?php

namespace App\Repositories\Provider\Contracts;

use Manowartop\ServiceRepositoryPattern\Repositories\Contracts\BaseRepositoryInterface;

/**
 * Interface GasProviderRepositoryInterface
 * @package App\Repositories\GasProvider\Contracts
 */
interface ProviderRepositoryInterface extends BaseRepositoryInterface
{

}
