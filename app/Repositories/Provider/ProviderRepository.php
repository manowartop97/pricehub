<?php

namespace App\Repositories\Provider;

use App\Models\Provider\Provider;
use App\Repositories\Provider\Contracts\ProviderRepositoryInterface;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Manowartop\ServiceRepositoryPattern\Repositories\BaseRepository;

/**
 * Class GasProviderRepository
 * @package App\Repositories\GasProvider
 */
class ProviderRepository extends BaseRepository implements ProviderRepositoryInterface
{
    /**
     * @var int
     */
    protected $cacheTtl= 99999;

    /**
     * @const
     */
    protected const PROVIDERS_MAPPING = [
        'gas-providers'    => Provider::TYPE_GAS,
        'energy-providers' => Provider::TYPE_ENERGY,
    ];

    /**
     * @var string
     */
    protected $modelClass = Provider::class;

    /**
     * @param array $search
     * @return Builder
     * @throws Exception
     */
    protected function getFilteredQuery(array $search = []): Builder
    {
        if (!isset($search['type']) || !isset(self::PROVIDERS_MAPPING[$search['type']])) {
            throw new Exception('Invalid type given for providers query');
        }

        return $this->getQuery()
            ->where('type', self::PROVIDERS_MAPPING[$search['type']])
            ->when(!empty($search['name'] ?? null), function (Builder $query) use ($search) {
                $query->where('name', 'like', "%{$search['name']}%");
            })
            ->when(!empty($search['index'] ?? null), function (Builder $query) use ($search) {
                $query->whereHas('locations', function (Builder $query) use ($search) {
                    $query->where('index', 'like', $search['index'] . '%');
                });
            });
    }
}
