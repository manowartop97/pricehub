<?php

namespace App\Repositories\SupplierRequest;

use App\Models\SupplierRequest\SupplierRequest;
use App\Repositories\SupplierRequest\Contracts\SupplierRequestRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Manowartop\ServiceRepositoryPattern\Repositories\BaseRepository;

/**
 * Class SupplierRequestRepository
 * @package App\Repositories\SupplierRequest
 */
class SupplierRequestRepository extends BaseRepository implements SupplierRequestRepositoryInterface
{
    /**
     * @var string
     */
    protected $modelClass = SupplierRequest::class;

    /**
     * @param array $search
     * @return Builder
     */
    protected function getFilteredQuery(array $search = []): Builder
    {
        return $this->getQuery()
            ->when(isset($search['type']), function (Builder $query) use ($search) {
                $query->where('type', $search['type']);
            });
    }
}
