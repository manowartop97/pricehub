<?php

namespace App\Repositories\SupplierRequest\Contracts;

use Manowartop\ServiceRepositoryPattern\Repositories\Contracts\BaseRepositoryInterface;

/**
 * Interface SupplierRequestRepositoryInterface
 * @package App\Repositories\SupplierRequest\Contracts
 */
interface SupplierRequestRepositoryInterface extends BaseRepositoryInterface
{

}
