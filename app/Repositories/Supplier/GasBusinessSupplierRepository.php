<?php

namespace App\Repositories\Supplier;

use App\Models\Supplier\GasBusinessSupplier;
use App\Repositories\Supplier\Contracts\GasBusinessSupplierRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Manowartop\ServiceRepositoryPattern\Repositories\BaseRepository;

/**
 * Class GasBusinessSupplierRepository
 * @package App\Repositories\Supplier
 */
class GasBusinessSupplierRepository extends BaseRepository implements GasBusinessSupplierRepositoryInterface
{
    /**
     * @var string
     */
    protected $modelClass = GasBusinessSupplier::class;

    /**
     * @param array $search
     * @return Builder
     */
    protected function getFilteredQuery(array $search = []): Builder
    {
        return $this->getQuery()
            ->with('supplierProviders')
            ->when(isset($search['location']), function (Builder $query) use ($search) {
                $query->whereHas('supplierProviders.providerLocations', function (Builder $query) use ($search) {
                    $query->where('location_id', $search['location']);
                });
            })
            ->when(isset($search['withFormula']) && $search['withFormula'], function (Builder $query) {
                $query->whereNotNull('formula');
            });
    }
}
