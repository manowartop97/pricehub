<?php

namespace App\Repositories\Supplier\Contracts;

use Manowartop\ServiceRepositoryPattern\Repositories\Contracts\BaseRepositoryInterface;

/**
 * Interface EnergyBusinessSupplierRepositoryInterface
 * @package App\Repositories\Supplier\Contracts
 */
interface EnergyBusinessSupplierRepositoryInterface extends BaseRepositoryInterface
{

}
