<?php

namespace App\Repositories\Supplier\Contracts;

use Manowartop\ServiceRepositoryPattern\Repositories\Contracts\BaseRepositoryInterface;

/**
 * Interface GasClientSupplierRepositoryInterface
 * @package App\Repositories\Supplier\Contracts
 */
interface GasClientSupplierRepositoryInterface extends BaseRepositoryInterface
{

}
