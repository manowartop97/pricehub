<?php

namespace App\Repositories\Supplier\Contracts;

use Manowartop\ServiceRepositoryPattern\Repositories\Contracts\BaseRepositoryInterface;

/**
 * Interface GasBusinessSupplierRepositoryInterface
 * @package App\Repositories\Supplier\Contracts
 */
interface GasBusinessSupplierRepositoryInterface extends BaseRepositoryInterface
{

}
