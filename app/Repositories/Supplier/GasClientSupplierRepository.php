<?php

namespace App\Repositories\Supplier;

use App\Models\Supplier\GasClientSupplier;
use App\Repositories\Supplier\Contracts\GasClientSupplierRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Manowartop\ServiceRepositoryPattern\Repositories\BaseRepository;

/**
 * Class GasClientSupplierRepository
 * @package App\Repositories\Supplier
 */
class GasClientSupplierRepository extends BaseRepository implements GasClientSupplierRepositoryInterface
{
    /**
     * @var string
     */
    protected $modelClass = GasClientSupplier::class;

    /**
     * @param array $search
     * @return Builder
     */
    protected function getFilteredQuery(array $search = []): Builder
    {
        return $this->getQuery()
            ->with(['supplierProviders', 'tariffs'])
            ->when(isset($search['location']), function (Builder $query) use ($search) {
                $query->whereHas('supplierProviders.providerLocations', function (Builder $query) use ($search) {
                    $query->where('location_id', $search['location']);
                });
            })
            ->when(isset($search['withFormula']) && $search['withFormula'], function (Builder $query) {
                $query->whereHas('tariffs');
            });
    }
}
