<?php

namespace App\Repositories\Location\Contracts;

use Manowartop\ServiceRepositoryPattern\Repositories\Contracts\BaseRepositoryInterface;

/**
 * Interface LocationRepositoryInterface
 * @package App\Repositories\Location\Contracts
 */
interface LocationRepositoryInterface extends BaseRepositoryInterface
{

}
