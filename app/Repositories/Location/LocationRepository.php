<?php

namespace App\Repositories\Location;

use App\Models\Location\Location;
use App\Repositories\Location\Contracts\LocationRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Manowartop\ServiceRepositoryPattern\Repositories\BaseRepository;

/**
 * Class LocationRepository
 * @package App\Repositories\Location
 */
class LocationRepository extends BaseRepository implements LocationRepositoryInterface
{
    /**
     * @var string
     */
    protected $modelClass = Location::class;

    /**
     * @param array $search
     * @return Builder
     */
    protected function getFilteredQuery(array $search = []): Builder
    {
        return $this->getQuery()
            ->when(isset($search['index']), function (Builder $query) use ($search) {
                $query->where('index', 'like', $search['index'].'%');
            });
    }
}
