<?php

namespace App\Models\SupplierRequest;

use App\Helpers\DataHelper;
use App\Models\Provider\Provider;
use App\Services\Supplier\Factory\Factory;
use Manowartop\ServiceRepositoryPattern\Models\BaseModel;

/**
 * Class SupplierRequest
 * @package App\Models\SupplierRequest
 *
 * @property integer $id
 * @property string $type
 * @property array $data
 */
class SupplierRequest extends BaseModel
{
    /**
     * @var string[]
     */
    protected $fillable = [
        'type',
        'data'
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'data' => 'array'
    ];

    /**
     * @param string $type
     * @param bool $isFullList
     * @return array
     */
    public static function getSupplierFields(string $type, bool $isFullList = false): array
    {
        $fields = [
            Factory::TYPE_GAS_CLIENT_SUPPLIER      => [
                [
                    'name'        => 'file_id',
                    'is_required' => true,
                    'rule'        => 'nullable|integer|exists:files,id' // TODO change
                ],
                [
                    'name'        => 'full_name',
                    'is_required' => true,
                    'rule'        => 'required|string|min:5'
                ],
                [
                    'name'        => 'eic_code',
                    'is_required' => true,
                    'rule'        => 'required'
                ],
                [
                    'name'        => 'phone',
                    'is_required' => true,
                    'rule'        => 'required|string'
                ],
                [
                    'name'        => 'email',
                    'is_required' => true,
                    'rule'        => 'required|email'
                ],
                [
                    'name'        => 'ipn',
                    'is_required' => true,
                    'rule'        => 'required'
                ],
                [
                    'name'        => 'is_privilege',
                    'is_required' => true,
                    'rule'        => 'required|boolean'
                ],
                [
                    'name'        => 'is_counter',
                    'is_required' => true,
                    'rule'        => 'required|boolean'
                ],
                [
                    'name'        => 'apartment_type',
                    'is_required' => true,
                    'rule'        => 'required|string|in:' . implode(',', DataHelper::APARTMENT_TYPE_DATA)
                ],
                [
                    'name'        => 'usage_type',
                    'is_required' => true,
                    'rule'        => 'required|string|in:' . implode(',', DataHelper::USAGE_TYPE)
                ],
//                [
//                    'name'        => 'provider_id',
//                    'is_required' => true,
//                    'rule'        => 'required|integer|exists:' . Provider::getTableName() . ',id'
//                ],
                [
                    'name'        => 'avg_yearly_usage_value',
                    'is_required' => true,
                    'rule'        => 'required|numeric'
                ],
                [
                    'name'        => 'full_address',
                    'is_required' => true,
                    'rule'        => 'required|string'
                ],
                [
                    'name'        => 'tariff',
                    'is_required' => true,
                    'rule'        => 'required'
                ],
            ],
            Factory::TYPE_GAS_BUSINESS_SUPPLIER    => [
                [
                    'name'        => 'file_id',
                    'is_required' => true,
                    'rule'        => 'nullable|integer|exists:files,id' // TODO change
                ],
                [
                    'name'        => 'full_name',
                    'is_required' => true,
                    'rule'        => 'required|string|min:5'
                ],
                [
                    'name'        => 'company_name',
                    'is_required' => true,
                    'rule'        => 'required|string'
                ],
                [
                    'name'        => 'eic_code',
                    'is_required' => true,
                    'rule'        => 'required'
                ],
                [
                    'name'        => 'phone',
                    'is_required' => true,
                    'rule'        => 'required|string'
                ],
                [
                    'name'        => 'email',
                    'is_required' => true,
                    'rule'        => 'required|email'
                ],
                [
                    'name'        => 'edpnou',
                    'is_required' => true,
                    'rule'        => 'required|string'
                ],
//                [
//                    'name'        => 'provider_id',
//                    'is_required' => true,
//                    'rule'        => 'required|integer|exists:' . Provider::getTableName() . ',id'
//                ],
                [
                    'name'        => 'avg_yearly_usage_value',
                    'is_required' => true,
                    'rule'        => 'required|numeric'
                ],
                [
                    'name'        => 'full_address',
                    'is_required' => true,
                    'rule'        => 'required|string'
                ],
            ],
            Factory::TYPE_ENERGY_BUSINESS_SUPPLIER => [
                [
                    'name'        => 'file_id',
                    'is_required' => true,
                    'rule'        => 'nullable|integer|exists:files,id' // TODO change
                ],
                [
                    'name'        => 'full_name',
                    'is_required' => true,
                    'rule'        => 'required|string|min:5'
                ],
                [
                    'name'        => 'company_name',
                    'is_required' => true,
                    'rule'        => 'required|string'
                ],
                [
                    'name'        => 'email',
                    'is_required' => true,
                    'rule'        => 'required|email'
                ],
                [
                    'name'        => 'edpnou',
                    'is_required' => true,
                    'rule'        => 'required|string'
                ],
//                [
//                    'name'        => 'provider_id',
//                    'is_required' => true,
//                    'rule'        => 'required|integer|exists:' . Provider::getTableName() . ',id'
//                ],
                [
                    'name'        => 'avg_yearly_usage_value',
                    'is_required' => true,
                    'rule'        => 'required|numeric'
                ],
                [
                    'name'        => 'full_address',
                    'is_required' => true,
                    'rule'        => 'required|string'
                ],
            ]
        ];

        if (!isset($fields[$type])) {
            return [];
        }

        if ($isFullList) {
            return $fields[$type] ?? [];
        }

        return collect($fields[$type])
            ->map(function ($data) {
                unset($data['rule']);
                return $data;
            })
            ->toArray();
    }
}
