<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Relations\MorphTo;
use Manowartop\ServiceRepositoryPattern\Models\BaseModel;

/**
 * Class File
 * @package App\Models\File
 *
 * @property integer $id
 * @property string $model_type
 * @property string $model_id
 * @property string $path
 * @property string $file_type
 */
class File extends BaseModel
{
    /**
     * @var string[]
     */
    protected $fillable = [
        'model_type',
        'model_id',
        'path',
        'file_type'
    ];

    /**
     * @return MorphTo
     */
    public function model(): MorphTo
    {
        return $this->morphTo();
    }
}
