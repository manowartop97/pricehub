<?php

namespace App\Models\Location;

use Manowartop\ServiceRepositoryPattern\Models\BaseModel;

/**
 * Class Street
 * @package App\Models\Location
 *
 */
class Street extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'up_streets';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function houses()
    {
        return $this->hasMany(House::class, 'street_id');
    }
}
