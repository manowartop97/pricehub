<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Manowartop\ServiceRepositoryPattern\Models\BaseModel;

/**
 * Class Location
 * @package App\Models\Location
 *
 * @property integer $id
 * @property string $index
 * @property string $name
 * @property float $osr_price
 * @property float $osp_price
 * @property float $exchange_price
 *
 * @property Region $region
 */
class Location extends BaseModel
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'up_locations';

    /**
     * @var string[]
     */
    protected $fillable = [
        'index',
        'name',
        'location_type',
        'region_name',
        'district_name'
    ];

    /**
     * @return BelongsTo
     */
    public function region(): BelongsTo
    {
        return $this->belongsTo(Region::class, 'region_name', 'name');
    }

    public function providerLocations()
    {

    }
}
