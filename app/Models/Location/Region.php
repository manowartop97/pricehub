<?php

namespace App\Models\Location;

use Manowartop\ServiceRepositoryPattern\Models\BaseModel;

/**
 * Class Region
 * @package App\Models\Location
 *
 * @property integer $id
 * @property string $name
 * @property float $exchange_price
 */
class Region extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'up_regions';

    /**
     * @var string[]
     */
    protected $fillable = [
        'id',
        'name',
        'exchange_price'
    ];
}
