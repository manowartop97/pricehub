<?php


namespace App\Models\Location;

use Manowartop\ServiceRepositoryPattern\Models\BaseModel;

/**
 * Class House
 * @package App\Models\Location
 */
class House extends BaseModel
{
    protected $table = 'up_houses';

    public function street()
    {
        return $this->belongsTo(Street::class, 'street_id', 'id');
    }
}
