<?php

namespace App\Models\Provider;

use App\Models\Location\Location;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Manowartop\ServiceRepositoryPattern\Models\BaseModel;

/**
 * Class GasProvider
 * @package App\Models\GasProvider
 *
 * @property integer $id
 * @property integer $type
 * @property string $name
 * @property string $information
 * @property float $osr_price
 * @property float $tariff
 *
 * @property Location[]|Collection $locations
 * @property ProviderHasLocation[]|Collection $providerLocations
 */
class Provider extends BaseModel
{
    /**
     * Provider types
     *
     * @const
     */
    const TYPE_GAS = 1;
    const TYPE_ENERGY = 2;

    /**
     * @var string[]
     */
    protected $fillable = [
        'name',
        'type',
        'tariff',
        'information'
    ];

    /**
     * @return HasMany
     */
    public function providerLocations(): HasMany
    {
        return $this->hasMany(ProviderHasLocation::class, 'provider_id');
    }

    /**
     * @return BelongsToMany
     */
    public function locations(): BelongsToMany
    {
        return $this->belongsToMany(
            Location::class,
            'provider_has_locations',
            'provider_id',
            'location_id'
        );
    }
}
