<?php

namespace App\Models\Provider;

use App\Models\Location\Location;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Class GasProviderHasLocation
 * @package App\Models\GasProvider
 *
 * @property integer $provider_id
 * @property integer $location_id
 *
 * @property Location $location
 * @property Provider $provider
 */
class ProviderHasLocation extends Pivot
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'provider_has_locations';

    /**
     * @var string[]
     */
    protected $primaryKey = ['provider_id', 'location_id'];

    /**
     * @var string
     */
    protected $keyType = 'array';

    /**
     * @var string[]
     */
    protected $fillable = ['provider_id', 'location_id'];

    /**
     * @return BelongsTo
     */
    public function location(): BelongsTo
    {
        return $this->belongsTo(Location::class);
    }

    /**
     * @return BelongsTo
     */
    public function provider(): BelongsTo
    {
        return $this->belongsTo(Provider::class, 'provider_id', 'provider_id');
    }
}
