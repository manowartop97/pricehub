<?php

namespace App\Models\Supplier;

use Carbon\Carbon;

/**
 * Class EnergyBusinessProposition
 * @package App\Models\Supplier\Proposition
 *
 * @property integer $id
 * @property integer $supplier_id
 * @property string $formula
 * @property float $margin
 * @property integer $tariff_type
 * @property string $name
 * @property string $features
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class EnergyBusinessSupplier extends BaseSupplierModel
{
    /**
     * @var array
     */
    protected $fillable = [
        'supplier_id',
        'name',
        'margin',
        'tariff_type',
        'formula',
        'features'
    ];
}
