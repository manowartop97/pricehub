<?php

namespace App\Models\Supplier;

use App\Models\File\File;
use App\Models\Supplier\Tariff\Tariff;
use App\Traits\File\HasFiles;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Manowartop\ServiceRepositoryPattern\Models\BaseModel;

/**
 * Class BaseSupplierModel
 * @package App\Models\Supplier
 *
 * @property float $result
 * @property string $formula
 * @property File[]|Collection $files
 * @property Tariff[]|Collection $tariffs
 * @property SupplierHasProvider[]|Collection $supplierProviders
 */
class BaseSupplierModel extends BaseModel
{
    use HasFiles;

    /**
     * @const
     */
    const TYPE_PENNY = 1;
    const TYPE_PERCENT = 2;

    /**
     * @var string[]
     */
    protected $casts = [
        'features' => 'array'
    ];

    /**
     * Logo file
     *
     * @return File|null
     */
    public function logo(): ?File
    {
        return $this->files->where('file_type', 'logo')->first();
    }

    /**
     * @return MorphMany
     */
    public function tariffs(): MorphMany
    {
        return $this->morphMany(Tariff::class, 'model');
    }

    /**
     * @return MorphMany
     */
    public function supplierProviders(): MorphMany
    {
        return $this->morphMany(SupplierHasProvider::class, 'supplier');
    }
}
