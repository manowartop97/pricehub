<?php

namespace App\Models\Supplier;

use App\Models\Provider\Provider;
use App\Models\Provider\ProviderHasLocation;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Manowartop\ServiceRepositoryPattern\Models\BaseModel;

/**
 * Class SupplierHasProvider
 * @package App\Models\Supplier
 *
 * @property integer $id
 * @property string $supplier_type
 * @property integer $supplier_id
 * @property integer $provider_id
 *
 * @property Provider $provider
 * @property BaseSupplierModel $supplier
 */
class SupplierHasProvider extends BaseModel
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'supplier_has_providers';

    /**
     * @var array
     */
    protected $fillable = [
        'supplier_type',
        'supplier_id',
        'provider_id',
    ];

    /**
     * @return BelongsTo
     */
    public function provider(): BelongsTo
    {
        return $this->belongsTo(Provider::class, 'provider_id');
    }

    /**
     * @return MorphTo
     */
    public function supplier(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @return HasMany
     */
    public function providerLocations(): HasMany
    {
        return $this->hasMany(ProviderHasLocation::class, 'provider_id', 'provider_id');
    }
}
