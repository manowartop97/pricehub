<?php

namespace App\Models\Supplier\Tariff;

use App\Models\Supplier\BaseSupplierModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Manowartop\ServiceRepositoryPattern\Models\BaseModel;

/**
 * Class Tariff
 * @package App\Models\Supplier\Tariff
 *
 * @property integer $id
 * @property string $name
 * @property float $price
 * @property string $formula
 * @property array $condition
 * @property string $model_type
 * @property integer $model_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property BaseSupplierModel $model
 */
class Tariff extends BaseModel
{
    /**
     * @var string[]
     */
    protected $fillable = [
        'name',
        'price',
        'formula',
        'condition',
        'model_type',
        'model_id'
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'condition' => 'array'
    ];

    /**
     * @return MorphTo
     */
    public function model(): MorphTo
    {
        return $this->morphTo();
    }
}
