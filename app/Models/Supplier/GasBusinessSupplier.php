<?php

namespace App\Models\Supplier;

use Carbon\Carbon;

/**
 * Class GasBusinessProposition
 * @package App\Models\Supplier\Proposition
 *
 * @property integer $id
 * @property integer $supplier_id
 * @property string $name
 * @property string $features
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class GasBusinessSupplier extends BaseSupplierModel
{
    /**
     * @var array
     */
    protected $fillable = [
        'supplier_id',
        'name',
        'features'
    ];
}
