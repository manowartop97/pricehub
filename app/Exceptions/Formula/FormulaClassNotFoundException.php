<?php

namespace App\Exceptions\Formula;

use Exception;
use Throwable;

/**
 * Class FormulaClassNotFoundException
 * @package App\Exceptions\Formula
 */
class FormulaClassNotFoundException extends Exception
{
    /**
     * FormulaClassNotFoundException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "Formula class not found", $code = 404, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
