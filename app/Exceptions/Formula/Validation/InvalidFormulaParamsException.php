<?php

namespace App\Exceptions\Formula\Validation;

use Exception;
use Throwable;

/**
 * Class InvalidFormulaParamsException
 * @package App\Exceptions\Formula\Validation
 */
class InvalidFormulaParamsException extends Exception
{
    /**
     * InvalidFormulaParamsException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "Formula params are not valid", $code = 422, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
