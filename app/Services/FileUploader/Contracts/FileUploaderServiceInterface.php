<?php

namespace App\Services\FileUploader\Contracts;

use Illuminate\Http\UploadedFile;

/**
 * Interface FileUploaderServiceInterface
 * @package App\Services\FileUploader\Contracts
 */
interface FileUploaderServiceInterface
{
    /**
     * Upload file
     *
     * @param UploadedFile $file
     * @param string|null $path
     * @param string|null $name
     * @return string
     */
    public function uploadFile(UploadedFile $file, string $path = null, string $name = null): string;

    /**
     * Upload many files
     *
     * @param array $files
     * @param string|null $path
     * @return array
     */
    public function uploadFiles(array $files = [], string $path = null): array;

    /**
     * Delete file
     *
     * @param string $path
     * @return void
     */
    public function deleteFile(string $path): void;

    /**
     * Delete files
     *
     * @param array $paths
     * @return void
     */
    public function deleteFiles(array $paths): void;
}
