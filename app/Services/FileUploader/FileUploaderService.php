<?php

namespace App\Services\FileUploader;

use App\Services\FileUploader\Contracts\FileUploaderServiceInterface;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * Class FileUploaderService
 * @package App\Services\FileUploader
 */
class FileUploaderService implements FileUploaderServiceInterface
{
    /**
     * Default save path
     *
     * @const
     */
    protected const DEFAULT_PATH = 'uploads';

    /**
     * Upload file
     *
     * @param UploadedFile $file
     * @param string|null $path
     * @param string|null $name
     * @return string
     */
    public function uploadFile(UploadedFile $file, string $path = null, string $name = null): string
    {
        $path = $path ?? self::DEFAULT_PATH;
        $name = $name ?? Str::random(10) . '.' . $file->clientExtension();

        $file->storeAs('public/' . $path, $name);

        return "{$path}/{$name}";
    }

    /**
     * Upload files
     *
     * @param array $files
     * @param string|null $path
     * @return array
     */
    public function uploadFiles(array $files = [], string $path = null): array
    {
        $paths = [];

        foreach ($files as $file) {
            $paths[] = $this->uploadFile($file, $path);
        }

        return $paths;
    }

    /**
     * Delete file
     *
     * @param string $path
     * @return void
     */
    public function deleteFile(string $path): void
    {
        Storage::delete('public/' . $path);
    }

    /**
     * Delete files
     *
     * @param array $paths
     * @return void
     */
    public function deleteFiles(array $paths): void
    {
        foreach ($paths as &$path) {
            $path .= 'public/' . $path;
        }

        Storage::delete($paths);
    }
}
