<?php

namespace App\Services\SupplierRequest;

use App\Models\File\File;
use App\Models\SupplierRequest\SupplierRequest;
use App\Repositories\SupplierRequest\Contracts\SupplierRequestRepositoryInterface;
use App\Services\FileUploader\Contracts\FileUploaderServiceInterface;
use App\Services\SupplierRequest\Contracts\SupplierRequestServiceInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Manowartop\ServiceRepositoryPattern\Services\BaseCrudService;

/**
 * Class SupplierRequestService
 * @package App\Services\SupplierRequest
 */
class SupplierRequestService extends BaseCrudService implements SupplierRequestServiceInterface
{
    /**
     * @var SupplierRequestRepositoryInterface
     */
    protected $repository = SupplierRequestRepositoryInterface::class;

    /**
     * @param array $data
     * @return Model|null
     */
    public function create(array $data): ?Model
    {
        $model = parent::create($data);

        File::query()->whereKey($data['file_id'])->update(['model_id' => $model->getKey()]);

        return $model;
    }

    /**
     * @param UploadedFile $file
     * @return int
     */
    public function uploadFile(UploadedFile $file): int
    {
        $fileModel = File::query()->create([
            'model_type' => SupplierRequest::class,
            'path'       => resolve(FileUploaderServiceInterface::class)->uploadFile($file),
        ]);

        return $fileModel->getKey();
    }
}
