<?php

namespace App\Services\SupplierRequest\Contracts;

use Illuminate\Http\UploadedFile;
use Manowartop\ServiceRepositoryPattern\Services\Contracts\BaseCrudServiceInterface;

/**
 * Interface SupplierRequestServiceInterface
 * @package App\Services\SupplierRequest\Contracts
 */
interface SupplierRequestServiceInterface extends BaseCrudServiceInterface
{
    /**
     * Upload tmp file and get id
     *
     * @param UploadedFile $file
     * @return int
     */
    public function uploadFile(UploadedFile $file):int;
}
