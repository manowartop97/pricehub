<?php

namespace App\Services\Location\Contracts;

use Manowartop\ServiceRepositoryPattern\Services\Contracts\BaseCrudServiceInterface;

/**
 * Interface LocationServiceInterface
 * @package App\Services\Location\Contracts
 */
interface LocationServiceInterface extends BaseCrudServiceInterface
{

}
