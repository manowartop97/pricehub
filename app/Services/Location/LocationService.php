<?php

namespace App\Services\Location;

use App\Repositories\Location\Contracts\LocationRepositoryInterface;
use App\Services\Location\Contracts\LocationServiceInterface;
use Manowartop\ServiceRepositoryPattern\Services\BaseCrudService;

/**
 * Class LocationService
 * @package App\Services\Location
 */
class LocationService extends BaseCrudService implements LocationServiceInterface
{
    /**
     * @var LocationRepositoryInterface
     */
    protected $repository = LocationRepositoryInterface::class;
}
