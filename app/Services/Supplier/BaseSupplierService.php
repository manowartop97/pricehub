<?php

namespace App\Services\Supplier;

use App\Models\File\File;
use App\Models\Supplier\BaseSupplierModel;
use App\Models\Supplier\EnergyBusinessSupplier;
use App\Models\Supplier\GasBusinessSupplier;
use App\Models\Supplier\GasClientSupplier;
use App\Services\FileUploader\Contracts\FileUploaderServiceInterface;
use App\Services\Location\Contracts\LocationServiceInterface;
use App\Services\Supplier\Contracts\BaseSupplierServiceInterface;
use App\Traits\Collection\PaginateCollection;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Manowartop\ServiceRepositoryPattern\Services\BaseCrudService;

/**
 * Class BaseSupplierService
 * @package App\Services\Supplier
 */
abstract class BaseSupplierService extends BaseCrudService implements BaseSupplierServiceInterface
{
    use PaginateCollection;

    /**
     * Get suppliers with pagination and calculated formulas
     *
     * @param array $search
     * @return LengthAwarePaginator
     */
    public function getAllCalculatedAndPaginated(array $search = []): LengthAwarePaginator
    {
        $location = resolve(LocationServiceInterface::class)->findOrFail($search['location']);

        $data = collect();

        /** @var BaseSupplierModel|GasClientSupplier|GasBusinessSupplier|EnergyBusinessSupplier $supplier */
        foreach ($this->getAll(array_merge(['withFormula' => true], $search)) as $supplier) {
            foreach ($supplier->tariffs as $tariff) {

                if (!is_null($tariff->condition)) {

                    if (!is_null($tariff->condition['from'] ?? null) && $tariff->condition['from'] > $search['kWh']) {
                        continue;
                    }

                    if (!is_null($tariff->condition['to'] ?? null) && $tariff->condition['to'] < $search['kWh']) {
                        continue;
                    }
                }

                if (!class_exists($tariff->formula)) {
                    continue;
                }

                $data[] = [
                    'name'         => $supplier->name,
                    'features'     => $supplier->features,
                    'created_at'   => optional($supplier->created_at)->timestamp,
                    'updated_at'   => optional($supplier->updated_at)->timestamp,
                    'tariff_name'  => $tariff->name,
                    'tariff_price' => resolve($tariff->formula)->calculate(array_merge(
                            $search,
                            ['location' => $location, 'model' => $tariff, 'supplier' => $supplier])
                    )
                ];
            }
        }

        return $this->paginate($data->sortBy('tariff_price'), 15);
    }

    /**
     * @param array $data
     * @return Model|null
     */
    public function create(array $data): ?Model
    {
        return DB::transaction(function () use ($data) {
            /**
             * @var BaseSupplierModel $model
             */
            $model = parent::create($data);

            $model->supplierProviders()->createMany(
                collect($data['providers'] ?? [])
                    ->map(function ($item) {
                        return ['provider_id' => $item];
                    })
            );

            $model->tariffs()->createMany($data['tariffs']);

            return $model->refresh();
        });
    }

    /**
     * @param BaseSupplierModel $keyOrModel
     * @param array $data
     * @return Model|null
     */
    public function update($keyOrModel, array $data): ?Model
    {
        return DB::transaction(function () use ($keyOrModel, $data) {

            /** @var BaseSupplierModel $model */
            $model = parent::update($keyOrModel, $data);

            $model->supplierProviders()->delete();
            $model->tariffs()->delete();

            $model->supplierProviders()->createMany(
                collect($data['providers'] ?? [])
                    ->map(function ($item) {
                        return ['provider_id' => $item];
                    })
            );

            $model->tariffs()->createMany($data['tariffs']);

            return $model->refresh();
        });
    }

    /**
     * Update supplier logo
     *
     * @param Model|BaseSupplierModel $model
     * @param UploadedFile $logo
     * @return Model
     * @throws Exception
     */
    public function updateLogo(Model $model, UploadedFile $logo): Model
    {
        return DB::transaction(function () use ($model, $logo) {
            $uploaderService = resolve(FileUploaderServiceInterface::class);

            /** @var File $logoModel */
            if (!is_null($logoModel = $model->files()->where('file_type', '=', 'logo')->first())) {
                $uploaderService->deleteFile($logoModel->path);
                $logoModel->delete();
            }

            $model->files()->create([
                'path'      => resolve(FileUploaderServiceInterface::class)->uploadFile($logo),
                'file_type' => 'logo'
            ]);

            return $model->refresh();
        });
    }

}
