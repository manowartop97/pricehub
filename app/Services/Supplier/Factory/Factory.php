<?php

namespace App\Services\Supplier\Factory;

use App\Services\Supplier\Contracts\BaseSupplierServiceInterface;
use App\Services\Supplier\Contracts\EnergyBusinessSupplierServiceInterface;
use App\Services\Supplier\Contracts\GasBusinessSupplierServiceInterface;
use App\Services\Supplier\Contracts\GasClientSupplierServiceInterface;
use App\Services\Supplier\Factory\Contracts\FactoryInterface;
use Exception;

/**
 * Class Factory
 * @package App\Services\Supplier\Factory
 */
class Factory implements FactoryInterface
{
    /**
     * @const
     */
    const TYPE_GAS_CLIENT_SUPPLIER = 'gas-client';
    const TYPE_GAS_BUSINESS_SUPPLIER = 'gas-business';
    const TYPE_ENERGY_BUSINESS_SUPPLIER = 'energy-business';

    /**
     * @const
     */
    const SUPPLIER_TYPES = [
        self::TYPE_ENERGY_BUSINESS_SUPPLIER,
        self::TYPE_GAS_CLIENT_SUPPLIER,
        self::TYPE_GAS_BUSINESS_SUPPLIER
    ];

    /**
     * Get supplier service by type
     *
     * @param string|null $type
     * @return BaseSupplierServiceInterface
     * @throws Exception
     */
    public function getService(string $type = null): BaseSupplierServiceInterface
    {
        switch ($type) {
            case self::TYPE_GAS_BUSINESS_SUPPLIER:
                return resolve(GasBusinessSupplierServiceInterface::class);
            case self::TYPE_GAS_CLIENT_SUPPLIER:
                return resolve(GasClientSupplierServiceInterface::class);
            case self::TYPE_ENERGY_BUSINESS_SUPPLIER:
                return resolve(EnergyBusinessSupplierServiceInterface::class);
            default:
                throw new Exception("Invalid supplier type {$type}");
        }
    }
}
