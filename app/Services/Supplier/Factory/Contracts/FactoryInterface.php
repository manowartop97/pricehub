<?php

namespace App\Services\Supplier\Factory\Contracts;

use App\Services\Supplier\Contracts\BaseSupplierServiceInterface;

/**
 * Interface FactoryInterface
 * @package App\Services\Supplier\Factory\Contracts
 */
interface FactoryInterface
{
    /**
     * Get service by type
     *
     * @param string|null $type
     * @return BaseSupplierServiceInterface
     */
    public function getService(string $type = null): BaseSupplierServiceInterface;
}
