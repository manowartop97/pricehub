<?php

namespace App\Services\Supplier\Contracts;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Manowartop\ServiceRepositoryPattern\Services\Contracts\BaseCrudServiceInterface;

/**
 * Interface BaseSupplierServiceInterface
 * @package App\Services\Supplier\Contracts
 */
interface BaseSupplierServiceInterface extends BaseCrudServiceInterface
{
    /**
     * Get resource for the data
     *
     * @return string
     */
    public function getResourceName(): string;

    /**
     * Get create request name
     *
     * @return string
     */
    public function getCreateRequestName(): string;

    /**
     * Get update request name
     *
     * @return string
     */
    public function getUpdateRequestName(): string;

    /**
     * Get search request name
     *
     * @return string
     */
    public function getSearchRequestName(): string;

    /**
     * Update supplier logo
     *
     * @param Model $model
     * @param UploadedFile $logo
     * @return Model
     */
    public function updateLogo(Model $model, UploadedFile $logo): Model;

    /**
     * Get suppliers with pagination and calculated formulas
     *
     * @param array $search
     * @return LengthAwarePaginator
     */
    public function getAllCalculatedAndPaginated(array $search = []): LengthAwarePaginator;
}
