<?php

namespace App\Services\Supplier\Formula\DefaultFormula;

use App\Exceptions\Formula\Validation\InvalidFormulaParamsException;
use App\Models\Location\Location;
use App\Services\Supplier\Formula\Base\BaseFormula;

/**
 * Class DefaultFormula
 * @package App\Services\Supplier\Formula\DefaultFormula
 */
class DefaultFormula extends BaseFormula
{
    /**
     * Default formula
     *
     * @param array $params
     * @return float
     * @throws InvalidFormulaParamsException
     */
    protected function calculateFormula(array $params): float
    {
        $this->validateParams($params);

        /** @var Location $location */
        $location = $params['location'];

        return round($params['kWh'] * ($this->getCoeficient($params['kWh']) + ($location->osr_price / 1000) + ($location->osp_price / 1000)), 2);
    }

    /**
     * Get coeficient
     *
     * @param int $kWh
     * @return float
     * @throws InvalidFormulaParamsException
     */
    protected function getCoeficient(int $kWh): float
    {
        switch ($kWh) {
            case $kWh < 20000 :
                return 1.90;
            case $kWh >= 20000 && $kWh < 50000 :
                return 1.80;
            case $kWh >= 50000 && $kWh < 100000 :
                return 1.75;
            case $kWh >= 100000 :
                return 1.70;
            default:
                throw new InvalidFormulaParamsException("Invalid kWh value");
        }
    }

    /**
     * Validate formula params
     *
     * @param array $params
     * @return void
     * @throws InvalidFormulaParamsException
     */
    protected function validateParams(array $params): void
    {
        if (!isset($params['kWh'])) {
            throw new InvalidFormulaParamsException("Missed required formula param 'kWh'");
        }

        if (!isset($params['location'])) {
            throw new InvalidFormulaParamsException("Missed required formula param 'location'");
        }
    }
}
