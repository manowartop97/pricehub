<?php

namespace App\Services\Supplier\Formula\Base;

use Throwable;

/**
 * Class BaseFormula
 * @package App\Services\Supplier\Formula\Base
 */
abstract class BaseFormula
{
    /**
     * Calculate formula
     *
     * @param array $params
     * @return float
     */
    public function calculate(array $params = []): float
    {
        if (empty($params)) {
            return 0;
        }
        try {
            return $this->calculateFormula($params);
        } catch (Throwable $exception) {
            return 0;
        }
    }

    /**
     * Concrete calculation implementation
     *
     * @param array $params
     * @return float
     */
    protected abstract function calculateFormula(array $params): float;

    /**
     * Validate params for formula
     *
     * @param array $params
     * @return void
     */
    protected abstract function validateParams(array $params): void;
}
