<?php

namespace App\Services\Supplier\Formula\EnergyProvidersFormula;

use App\Exceptions\Formula\Validation\InvalidFormulaParamsException;
use App\Models\Provider\Provider;
use App\Models\Location\Location;
use App\Models\Supplier\BaseSupplierModel;
use App\Models\Supplier\EnergyBusinessSupplier;
use App\Services\Supplier\Formula\Base\BaseFormula;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class GasProvidersFormula
 * @package App\Services\Supplier\Formula\GasProvidersFormula
 */
class EnergyProvidersFormula extends BaseFormula
{
    /**
     * Default formula
     *
     * @param array $params
     * @return float
     * @throws InvalidFormulaParamsException
     */
    protected function calculateFormula(array $params): float
    {
        $this->validateParams($params);

        /**
         * @var Location $location
         * @var EnergyBusinessSupplier $supplier
         */
        $location = $params['location'];
        $supplier = $params['model'];

        /** @var Provider|null $provider */
        $provider = Provider::query()
            ->where('type', Provider::TYPE_ENERGY)
            ->whereHas('providerLocations', function (Builder $query) use ($location) {
                $query->where('location_id', $location->getKey());
            })->first();

        if (is_null($provider)) {
            return 0;
        }

        if ($supplier->tariff_type == BaseSupplierModel::TYPE_PENNY) {

           return
               $params['kWh'] *
               (
                   ($provider->tariff / 1000)
                   + (env('OSP_PRICE_VALUE') / 1000)
                   + ($location->region->exchange_price / 1000)
                   + $supplier->margin
                   + (env('MARKET_OPERATOR_TARIFF') / 1000)
               );
        }

        $result = (($provider->tariff / 1000) + (env('MARKET_OPERATOR_TARIFF') / 1000) + (env('OSP_PRICE_VALUE') / 1000) + ($location->region->exchange_price / 1000));
        $margin = $result * ($supplier->margin / 100);

        return $params['kWh'] * ($result + $margin);
    }

    /**
     * Validate formula params
     *
     * @param array $params
     * @return void
     * @throws InvalidFormulaParamsException
     */
    protected function validateParams(array $params): void
    {
        if (!isset($params['kWh'])) {
            throw new InvalidFormulaParamsException("Missed required formula param 'kWh'");
        }

        if (!isset($params['location'])) {
            throw new InvalidFormulaParamsException("Missed required formula param 'location'");
        }
    }
}
