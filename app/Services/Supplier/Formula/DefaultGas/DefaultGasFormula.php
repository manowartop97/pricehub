<?php


namespace App\Services\Supplier\Formula\DefaultGas;

use App\Exceptions\Formula\Validation\InvalidFormulaParamsException;
use App\Services\Supplier\Formula\Base\BaseFormula;

/**
 * Class DefaultGasFormula
 * @package App\Services\Supplier\Formula\DefaultGas
 */
class DefaultGasFormula extends BaseFormula
{
    /**
     * @param array $params
     * @return float
     */
    protected function calculateFormula(array $params): float
    {
        return $params['kWh'] * $params['model']->price;
    }

    /**
     * @param array $params
     * @throws InvalidFormulaParamsException
     */
    protected function validateParams(array $params): void
    {
        if (!isset($params['kWh'])) {
            throw new InvalidFormulaParamsException("Missed required formula param 'kWh'");
        }

        if (!isset($params['location'])) {
            throw new InvalidFormulaParamsException("Missed required formula param 'location'");
        }
    }
}
