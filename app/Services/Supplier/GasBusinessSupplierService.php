<?php

namespace App\Services\Supplier;

use App\Http\Requests\Supplier\CreateGasBusinessSupplierRequest;
use App\Http\Requests\Supplier\SearchGasBusinessRequest;
use App\Http\Requests\Supplier\UpdateGasBusinessSupplierRequest;
use App\Http\Resources\Supplier\GasBusinessSupplierResource;
use App\Repositories\Supplier\Contracts\GasBusinessSupplierRepositoryInterface;
use App\Services\Supplier\Contracts\GasBusinessSupplierServiceInterface;

/**
 * Class GasBusinessSupplierService
 * @package App\Services\Supplier
 */
class GasBusinessSupplierService extends BaseSupplierService implements GasBusinessSupplierServiceInterface
{
    /**
     * @var GasBusinessSupplierRepositoryInterface
     */
    protected $repository = GasBusinessSupplierRepositoryInterface::class;

    /**
     * @return string
     */
    public function getResourceName(): string
    {
        return GasBusinessSupplierResource::class;
    }

    /**
     * @return string
     */
    public function getCreateRequestName(): string
    {
        return CreateGasBusinessSupplierRequest::class;
    }

    /**
     * @return string
     */
    public function getUpdateRequestName(): string
    {
        return UpdateGasBusinessSupplierRequest::class;
    }

    /**
     * @return string
     */
    public function getSearchRequestName(): string
    {
        return SearchGasBusinessRequest::class;
    }
}
