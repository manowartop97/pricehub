<?php

namespace App\Services\Supplier;

use App\Http\Requests\Supplier\CreateEnergyBusinessRequest;
use App\Http\Requests\Supplier\SearchEnergyBusinessRequest;
use App\Http\Requests\Supplier\UpdateEnergyBusinessRequest;
use App\Http\Resources\Supplier\EnergyBusinessSupplierResource;
use App\Repositories\Supplier\Contracts\EnergyBusinessSupplierRepositoryInterface;
use App\Services\Supplier\Contracts\EnergyBusinessSupplierServiceInterface;

/**
 * Class EnergyBusinessSupplierService
 * @package App\Services\Supplier
 */
class EnergyBusinessSupplierService extends BaseSupplierService implements EnergyBusinessSupplierServiceInterface
{
    /**
     * @var EnergyBusinessSupplierRepositoryInterface
     */
    protected $repository = EnergyBusinessSupplierRepositoryInterface::class;

    /**
     * @return string
     */
    public function getResourceName(): string
    {
        return EnergyBusinessSupplierResource::class;
    }

    /**
     * @return string
     */
    public function getCreateRequestName(): string
    {
        return CreateEnergyBusinessRequest::class;
    }

    /**
     * @return string
     */
    public function getUpdateRequestName(): string
    {
        return UpdateEnergyBusinessRequest::class;
    }

    /**
     * @return string
     */
    public function getSearchRequestName(): string
    {
        return SearchEnergyBusinessRequest::class;
    }
}
