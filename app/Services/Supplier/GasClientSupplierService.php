<?php

namespace App\Services\Supplier;

use App\Http\Requests\Supplier\CreateGasClientSupplierRequest;
use App\Http\Requests\Supplier\SearchGasClientRequest;
use App\Http\Requests\Supplier\UpdateGasClientSupplierRequest;
use App\Http\Resources\Supplier\GasClientSupplierResource;
use App\Repositories\Supplier\Contracts\GasClientSupplierRepositoryInterface;
use App\Services\Supplier\Contracts\GasClientSupplierServiceInterface;

/**
 * Class GasClientSupplierService
 * @package App\Services\Supplier
 */
class GasClientSupplierService extends BaseSupplierService implements GasClientSupplierServiceInterface
{
    /**
     * @var GasClientSupplierRepositoryInterface
     */
    protected $repository = GasClientSupplierRepositoryInterface::class;

    /**
     * @return string
     */
    public function getResourceName(): string
    {
        return GasClientSupplierResource::class;
    }

    /**
     * @return string
     */
    public function getCreateRequestName(): string
    {
        return CreateGasClientSupplierRequest::class;
    }

    /**
     * @return string
     */
    public function getUpdateRequestName(): string
    {
        return UpdateGasClientSupplierRequest::class;
    }

    /**
     * @return string
     */
    public function getSearchRequestName(): string
    {
        return SearchGasClientRequest::class;
    }
}
