<?php

namespace App\Services\Provider;

use App\Models\Provider\Provider;
use App\Repositories\Provider\Contracts\ProviderRepositoryInterface;
use App\Services\Provider\Contracts\ProviderServiceInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Manowartop\ServiceRepositoryPattern\Services\BaseCrudService;

/**
 * Class GasProviderService
 * @package App\Services\GasProvider
 */
class ProviderService extends BaseCrudService implements ProviderServiceInterface
{
    /**
     * @var ProviderRepositoryInterface
     */
    protected $repository = ProviderRepositoryInterface::class;

    /**
     * @param array $data
     * @return Model|null
     */
    public function create(array $data): ?Model
    {
        return DB::transaction(function () use ($data) {
            /** @var Provider $model */
            $model = parent::create($data);

            $this->attachLocations($model, $data['locations']);

            return $model->refresh();
        });
    }

    /**
     * @param Provider $keyOrModel
     * @param array $data
     * @return Model|null
     */
    public function update($keyOrModel, array $data): ?Model
    {
        return DB::transaction(function () use ($keyOrModel, $data) {
            /** @var Provider $model */
            $model = parent::update($keyOrModel, $data);

            $this->attachLocations($model, $data['locations']);

            return $model->refresh();
        });
    }

    /**
     * Attach locations to provider
     *
     * @param Model|Provider $model
     * @param array $locations
     */
    protected function attachLocations(Model $model, array $locations = []): void
    {
        $model->providerLocations()->delete();

        foreach ($locations as $locationId) {
            $model->providerLocations()->create(['location_id' => $locationId]);
        }
    }
}
