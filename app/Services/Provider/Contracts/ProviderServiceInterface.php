<?php

namespace App\Services\Provider\Contracts;

use Manowartop\ServiceRepositoryPattern\Services\Contracts\BaseCrudServiceInterface;

/**
 * Interface GasProviderServiceInterface
 * @package App\Services\GasProvider\Contracts
 */
interface ProviderServiceInterface extends BaseCrudServiceInterface
{

}
