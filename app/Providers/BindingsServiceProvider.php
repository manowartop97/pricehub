<?php

namespace App\Providers;

use App\Http\Requests\Supplier\CreateSupplierRequest;
use App\Http\Requests\Supplier\SearchSupplierRequest;
use App\Http\Requests\Supplier\UpdateSupplierRequest;
use App\Models\Supplier\BaseSupplierModel;
use App\Models\Supplier\EnergyBusinessSupplier;
use App\Models\Supplier\GasBusinessSupplier;
use App\Models\Supplier\GasClientSupplier;
use App\Models\Supplier\Tariff\Tariff;
use App\Observers\Supplier\EnergyBusinessSupplierObserver;
use App\Observers\Supplier\GasBusinessSupplierObserver;
use App\Observers\Supplier\GasClientSupplierObserver;
use App\Observers\TariffObserver;
use App\Repositories\Provider\Contracts\ProviderRepositoryInterface;
use App\Repositories\Provider\ProviderRepository;
use App\Repositories\Location\Contracts\LocationRepositoryInterface;
use App\Repositories\Location\LocationRepository;
use App\Repositories\Supplier\Contracts\EnergyBusinessSupplierRepositoryInterface;
use App\Repositories\Supplier\Contracts\GasBusinessSupplierRepositoryInterface;
use App\Repositories\Supplier\Contracts\GasClientSupplierRepositoryInterface;
use App\Repositories\Supplier\EnergyBusinessSupplierRepository;
use App\Repositories\Supplier\GasBusinessSupplierRepository;
use App\Repositories\Supplier\GasClientSupplierRepository;
use App\Repositories\SupplierRequest\Contracts\SupplierRequestRepositoryInterface;
use App\Repositories\SupplierRequest\SupplierRequestRepository;
use App\Services\FileUploader\Contracts\FileUploaderServiceInterface;
use App\Services\FileUploader\FileUploaderService;
use App\Services\Provider\Contracts\ProviderServiceInterface;
use App\Services\Provider\ProviderService;
use App\Services\Location\Contracts\LocationServiceInterface;
use App\Services\Location\LocationService;
use App\Services\Supplier\Contracts\BaseSupplierServiceInterface;
use App\Services\Supplier\Contracts\EnergyBusinessSupplierServiceInterface;
use App\Services\Supplier\Contracts\GasBusinessSupplierServiceInterface;
use App\Services\Supplier\Contracts\GasClientSupplierServiceInterface;
use App\Services\Supplier\EnergyBusinessSupplierService;
use App\Services\Supplier\Factory\Contracts\FactoryInterface;
use App\Services\Supplier\Factory\Factory;
use App\Services\Supplier\Formula\DefaultFormula\DefaultFormula;
use App\Services\Supplier\GasBusinessSupplierService;
use App\Services\Supplier\GasClientSupplierService;
use App\Services\SupplierRequest\Contracts\SupplierRequestServiceInterface;
use App\Services\SupplierRequest\SupplierRequestService;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;

class BindingsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        /*
         * Observers
         */
        EnergyBusinessSupplier::observe(EnergyBusinessSupplierObserver::class);
        GasClientSupplier::observe(GasClientSupplierObserver::class);
        GasBusinessSupplier::observe(GasBusinessSupplierObserver::class);
        Tariff::observe(TariffObserver::class);

        /**
         * Resolve service by route pram
         */
        $this->app->alias('supplierService', BaseSupplierServiceInterface::class);
        $this->app->bind('supplierService', function () {
            return resolve(FactoryInterface::class)->getService(Request::route('supplierService'));
        });

        /**
         * Resolve model by type and id
         */
        $this->app->alias('supplierModel', BaseSupplierModel::class);
        $this->app->bind('supplierModel', function () {
            return resolve(BaseSupplierServiceInterface::class)->findOrFail(Request::route('supplierModel'));
        });

        /**
         * Resolve model create request
         */
        $this->app->alias('createSupplierRequest', CreateSupplierRequest::class);
        $this->app->bind('createSupplierRequest', function () {
            return resolve(resolve(BaseSupplierServiceInterface::class)->getCreateRequestName());
        });

        /**
         * Resolve model update request
         */
        $this->app->alias('updateSupplierRequest', UpdateSupplierRequest::class);
        $this->app->bind('updateSupplierRequest', function () {
            return resolve(resolve(BaseSupplierServiceInterface::class)->getUpdateRequestName());
        });

        /**
         * Resolve model search request
         */
        $this->app->alias('searchSupplierRequest', SearchSupplierRequest::class);
        $this->app->bind('searchSupplierRequest', function () {
            return resolve(resolve(BaseSupplierServiceInterface::class)->getSearchRequestName());
        });

        /*
         * Repositories
         */
        $this->app->singleton(LocationRepositoryInterface::class, LocationRepository::class);
        $this->app->singleton(GasClientSupplierRepositoryInterface::class, GasClientSupplierRepository::class);
        $this->app->singleton(GasBusinessSupplierRepositoryInterface::class, GasBusinessSupplierRepository::class);
        $this->app->singleton(EnergyBusinessSupplierRepositoryInterface::class, EnergyBusinessSupplierRepository::class);
        $this->app->singleton(ProviderRepositoryInterface::class, ProviderRepository::class);
        $this->app->singleton(SupplierRequestRepositoryInterface::class, SupplierRequestRepository::class);

        /*
         * Services
         */
        $this->app->singleton(LocationServiceInterface::class, LocationService::class);
        $this->app->singleton(GasClientSupplierServiceInterface::class, GasClientSupplierService::class);
        $this->app->singleton(GasBusinessSupplierServiceInterface::class, GasBusinessSupplierService::class);
        $this->app->singleton(EnergyBusinessSupplierServiceInterface::class, EnergyBusinessSupplierService::class);
        $this->app->singleton(ProviderServiceInterface::class, ProviderService::class);
        $this->app->singleton(FactoryInterface::class, Factory::class);
        $this->app->singleton(FileUploaderServiceInterface::class, FileUploaderService::class);
        $this->app->singleton(SupplierRequestServiceInterface::class, SupplierRequestService::class);

        /*
         * Formulas
         */
        $this->app->singleton(DefaultFormula::class, DefaultFormula::class);
    }
}
