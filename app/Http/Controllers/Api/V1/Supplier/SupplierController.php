<?php

namespace App\Http\Controllers\Api\V1\Supplier;

use App\Http\Controllers\Controller;
use App\Http\Requests\Supplier\CreateSupplierRequest;
use App\Http\Requests\Supplier\SearchSupplierRequest;
use App\Http\Requests\Supplier\UpdateLogoRequest;
use App\Http\Requests\Supplier\UpdateSupplierRequest;
use App\Http\Resources\Supplier\EnergyBusinessSupplierResource;
use App\Http\Resources\Supplier\GasBusinessSupplierResource;
use App\Http\Resources\Supplier\GasClientSupplierResource;
use App\Http\Resources\SupplierRequest\SupplierRequestResource;
use App\Models\Supplier\BaseSupplierModel;
use App\Models\SupplierRequest\SupplierRequest;
use App\Services\Supplier\Contracts\BaseSupplierServiceInterface;
use App\Services\SupplierRequest\Contracts\SupplierRequestServiceInterface;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response as ResponseStatus;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

/**
 * Class SupplierController
 * @package App\Http\Controllers\Api\V1\Supplier
 */
class SupplierController extends Controller
{
    /**
     * @param string $method
     * @param array $parameters
     * @return SymfonyResponse
     */
    public function callAction($method, $parameters)
    {
        unset($parameters['supplierService']);

        return parent::callAction($method, $parameters);
    }

    /**
     * Show model
     *
     * @param SupplierRequestServiceInterface $service
     * @param string $type
     * @return AnonymousResourceCollection
     */
    public function getRequests(SupplierRequestServiceInterface $service, string $type): AnonymousResourceCollection
    {
        return SupplierRequestResource::collection($service->getAllPaginated(['type' => $type]));
    }

    /**
     * Get suppliers list
     *
     * @param BaseSupplierServiceInterface $supplierService
     * @return AnonymousResourceCollection
     */
    public function index(BaseSupplierServiceInterface $supplierService): AnonymousResourceCollection
    {
        return call_user_func(
            $supplierService->getResourceName() . '::collection',
            $supplierService->getAllPaginated()
        );
    }

    /**
     * Show model
     *
     * @param BaseSupplierModel $supplierModel
     * @param BaseSupplierServiceInterface $supplierService
     * @return GasClientSupplierResource|EnergyBusinessSupplierResource|GasBusinessSupplierResource
     */
    public function show(BaseSupplierServiceInterface $supplierService, BaseSupplierModel $supplierModel)
    {
        return call_user_func($supplierService->getResourceName() . '::make', $supplierModel);
    }

    /**
     * Store supplier
     *
     * @param CreateSupplierRequest $createSupplierRequest
     * @param BaseSupplierServiceInterface $supplierService
     * @return GasClientSupplierResource|EnergyBusinessSupplierResource|GasBusinessSupplierResource
     */
    public function store(CreateSupplierRequest $createSupplierRequest, BaseSupplierServiceInterface $supplierService)
    {
        return call_user_func(
            $supplierService->getResourceName() . '::make',
            $supplierService->create($createSupplierRequest->validated())
        );
    }

    /**
     * Update supplier
     *
     * @param UpdateSupplierRequest $updateSupplierRequest
     * @param BaseSupplierModel $supplierModel
     * @param BaseSupplierServiceInterface $supplierService
     * @return GasClientSupplierResource|EnergyBusinessSupplierResource|GasBusinessSupplierResource
     */
    public function update(
        UpdateSupplierRequest $updateSupplierRequest,
        BaseSupplierServiceInterface $supplierService,
        BaseSupplierModel $supplierModel
    )
    {
        return call_user_func(
            $supplierService->getResourceName() . '::make',
            $supplierService->update($supplierModel, $updateSupplierRequest->validated())
        );
    }

    /**
     * Update logo
     *
     * @param BaseSupplierModel $supplierModel
     * @param UpdateLogoRequest $request
     * @param BaseSupplierServiceInterface $supplierService
     * @return GasClientSupplierResource|EnergyBusinessSupplierResource|GasBusinessSupplierResource
     */
    public function updateLogo(
        UpdateLogoRequest $request,
        BaseSupplierServiceInterface $supplierService,
        BaseSupplierModel $supplierModel
    )
    {
        return call_user_func(
            $supplierService->getResourceName() . '::make',
            $supplierService->updateLogo($supplierModel, $request->file('logo'))
        );
    }

    /**
     * Destroy supplier
     *
     * @param BaseSupplierModel $supplierModel
     * @param BaseSupplierServiceInterface $supplierService
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(BaseSupplierServiceInterface $supplierService, BaseSupplierModel $supplierModel): JsonResponse
    {
        $supplierService->delete($supplierModel);

        return Response::json(null, ResponseStatus::HTTP_NO_CONTENT);
    }

    /**
     * Calculate suppliers variants
     *
     * @param SearchSupplierRequest $searchSupplierRequest
     * @param BaseSupplierServiceInterface $supplierService
     * @return AnonymousResourceCollection
     */
    public function calculate(SearchSupplierRequest $searchSupplierRequest, BaseSupplierServiceInterface $supplierService): AnonymousResourceCollection
    {
        return call_user_func(
            $supplierService->getResourceName() . '::collection',
            $supplierService->getAllCalculatedAndPaginated($searchSupplierRequest->validated())
        );
    }

    /**
     * Get supplier form fields
     *
     * @param string $type
     * @return JsonResponse
     */
    public function getFields(string $type): JsonResponse
    {
        return Response::json(['data' => SupplierRequest::getSupplierFields($type)], ResponseStatus::HTTP_OK);
    }
}
