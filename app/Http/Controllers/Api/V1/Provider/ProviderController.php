<?php

namespace App\Http\Controllers\Api\V1\Provider;

use App\Http\Controllers\Controller;
use App\Http\Requests\GasProvider\CreateRequest;
use App\Http\Requests\GasProvider\SearchRequest;
use App\Http\Requests\GasProvider\UpdateRequest;
use App\Http\Resources\Provider\ProviderResource;
use App\Models\Provider\Provider;
use App\Services\Provider\Contracts\ProviderServiceInterface;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response as ResponseStatus;
use Illuminate\Support\Facades\Response;

/**
 * Class GasProviderController
 * @package App\Http\Controllers\Api\V1\GasProvider
 */
class ProviderController extends Controller
{
    /**
     * @var ProviderServiceInterface
     */
    protected $service;

    /**
     * GasProviderController constructor.
     * @param ProviderServiceInterface $service
     */
    public function __construct(ProviderServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Get list of providers
     *
     * @param string $type
     * @param SearchRequest $request
     * @return AnonymousResourceCollection
     */
    public function index(string $type, SearchRequest $request): AnonymousResourceCollection
    {
        return ProviderResource::collection(
            $this->service->getAll(array_merge($request->validated(), ['type' => $type]))
        );
    }

    /**
     * Show provider
     *
     * @param Provider $provider
     * @return ProviderResource
     */
    public function show(Provider $provider): ProviderResource
    {
        return ProviderResource::make($provider);
    }

    /**
     * Store provider
     *
     * @param CreateRequest $request
     * @return ProviderResource
     */
    public function store(CreateRequest $request): ProviderResource
    {
        return ProviderResource::make($this->service->create($request->validated()));
    }

    /**
     * Update request
     *
     * @param Provider $provider
     * @param UpdateRequest $request
     * @return ProviderResource
     */
    public function update(Provider $provider, UpdateRequest $request): ProviderResource
    {
        return ProviderResource::make($this->service->update($provider, $request->validated()));
    }

    /**
     * Delete provider
     *
     * @param Provider $provider
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Provider $provider): JsonResponse
    {
        $this->service->delete($provider);

        return Response::json(null, ResponseStatus::HTTP_NO_CONTENT);
    }
}
