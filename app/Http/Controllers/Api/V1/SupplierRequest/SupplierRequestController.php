<?php

namespace App\Http\Controllers\Api\V1\SupplierRequest;

use App\Helpers\DataHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\SupplierRequest\StoreRequest;
use App\Http\Requests\SupplierRequest\UploadFileRequest;
use App\Http\Resources\SupplierRequest\SupplierRequestResource;
use App\Services\SupplierRequest\Contracts\SupplierRequestServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

/**
 * Class SupplierRequestController
 * @package App\Http\Controllers\Api\V1\SupplierRequest
 */
class SupplierRequestController extends Controller
{
    /**
     * @var SupplierRequestServiceInterface
     */
    protected $service;

    /**
     * SupplierRequestController constructor.
     * @param SupplierRequestServiceInterface $service
     */
    public function __construct(SupplierRequestServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @param StoreRequest $request
     * @return SupplierRequestResource
     */
    public function store(StoreRequest $request): SupplierRequestResource
    {
        return SupplierRequestResource::make(
            $this->service->create([
                'type'    => $request->get('type'),
                'file_id' => $request->get('file_id'),
                'data'    => $request->except('type')
            ])
        );
    }

    /**
     * @return JsonResponse
     */
    public function getAdditionalData(): JsonResponse
    {
        return Response::json([
            'data' => [
                'apartment_types' => DataHelper::APARTMENT_TYPE_DATA,
                'usage_types'     => DataHelper::USAGE_TYPE,
                'tariff_data'     => DataHelper::TARIFF_DATA
            ]
        ]);
    }

    /**
     * Upload file
     *
     * @param UploadFileRequest $request
     * @return JsonResponse
     */
    public function uploadFile(UploadFileRequest $request): JsonResponse
    {
        return Response::json(['data' => ['id' => $this->service->uploadFile($request->file('file'))]]);
    }
}
