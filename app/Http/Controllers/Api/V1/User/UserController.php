<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\UserResource;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserController
 * @package App\Http\Controllers\Api\V1\User
 */
class UserController extends Controller
{
    /**
     * Get auth user
     *
     * @return UserResource
     */
    public function me(): UserResource
    {
        return UserResource::make(Auth::user());
    }
}
