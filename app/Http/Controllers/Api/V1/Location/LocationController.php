<?php

namespace App\Http\Controllers\Api\V1\Location;

use App\Http\Controllers\Controller;
use App\Http\Requests\Location\CreateRequest;
use App\Http\Requests\Location\SearchRequest;
use App\Http\Requests\Location\UpdateRequest;
use App\Http\Resources\Location\LocationResource;
use App\Models\Location\Location;
use App\Services\Location\Contracts\LocationServiceInterface;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response as ResponseStatus;
use Illuminate\Support\Facades\Response;

/**
 * Class LocationController
 * @package App\Http\Controllers\Api\V1\Location
 */
class LocationController extends Controller
{
    /**
     * @var LocationServiceInterface
     */
    protected $service;

    /**
     * LocationController constructor.
     * @param LocationServiceInterface $service
     */
    public function __construct(LocationServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Get locations
     *
     * @param SearchRequest $request
     * @return AnonymousResourceCollection
     */
    public function index(SearchRequest $request): AnonymousResourceCollection
    {
        return LocationResource::collection($this->service->getAllPaginated($request->validated()));
    }

    /**
     * Store location
     *
     * @param CreateRequest $request
     * @return LocationResource
     */
    public function store(CreateRequest $request): LocationResource
    {
        return LocationResource::make($this->service->create($request->validated()));
    }

    /**
     * Show location
     *
     * @param Location $location
     * @return LocationResource
     */
    public function show(Location $location): LocationResource
    {
        return LocationResource::make($location);
    }

    /**
     * Update location
     *
     * @param Location $location
     * @param UpdateRequest $request
     * @return LocationResource
     */
    public function update(Location $location, UpdateRequest $request): LocationResource
    {
        return LocationResource::make($this->service->update($location, $request->validated()));
    }

    /**
     * Destroy location
     *
     * @param Location $location
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Location $location): JsonResponse
    {
        $this->service->delete($location);

        return Response::json(null, ResponseStatus::HTTP_NO_CONTENT);
    }
}
