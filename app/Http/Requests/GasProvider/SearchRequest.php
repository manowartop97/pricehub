<?php

namespace App\Http\Requests\GasProvider;

use App\Http\Requests\Request;

/**
 * Class SearchRequest
 * @package App\Http\Requests\GasProvider
 */
class SearchRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'  => 'nullable|string',
            'index' => 'nullable|numeric'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}
