<?php

namespace App\Http\Requests\GasProvider;

use App\Http\Requests\Request;
use App\Models\Provider\Provider;
use App\Models\Location\Location;

/**
 * Class UpdateRequest
 * @package App\Http\Requests\GasProvider
 */
class UpdateRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'type'        => 'required|integer|in:1,2',
            'name'        => 'required|string',
            'information' => 'nullable|string',
            'tariff'      => 'required|numeric',
            'locations'   => 'required|array|min:1',
            'locations.*' => 'required|integer|exists:' . Location::getTableName() . ',id'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}
