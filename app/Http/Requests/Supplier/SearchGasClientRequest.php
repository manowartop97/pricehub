<?php

namespace App\Http\Requests\Supplier;


use App\Models\Provider\Provider;
use App\Models\Location\Location;

/**
 * Class SearchGasClientRequest
 * @package App\Http\Requests\Supplier
 */
class SearchGasClientRequest extends SearchSupplierRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'kWh'          => 'required|numeric',
            'location'     => 'required|integer|exists:' . Location::getTableName() . ',id',
            'gas_provider' => 'nullable|integer|exists:' . Provider::getTableName() . ',id'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}
