<?php

namespace App\Http\Requests\Supplier;

use App\Models\Provider\Provider;
use App\Models\Supplier\BaseSupplierModel;

/**
 * Class CreateEnergyBusinessRequest
 * @package App\Http\Requests\Supplier
 */
class CreateEnergyBusinessRequest extends CreateSupplierRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'        => 'required|string',
            'formula'     => 'nullable|string',
            'margin'      => 'nullable|numeric',
            'tariff_type' => 'required|integer|in:' . BaseSupplierModel::TYPE_PERCENT . ',' . BaseSupplierModel::TYPE_PENNY,
            'providers'   => 'required|array',
            'providers.*' => 'required|integer|exists:' . Provider::getTableName() . ',id',
            'features'    => 'nullable|array',
            'tariffs'           => 'required|array',
            'tariffs.*.name'    => 'required|string',
            'tariffs.*.price'   => 'required|numeric',
            'tariffs.*.formula' => 'nullable|string',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}
