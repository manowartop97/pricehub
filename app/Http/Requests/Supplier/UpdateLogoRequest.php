<?php

namespace App\Http\Requests\Supplier;

use App\Http\Requests\Request;

/**
 * Class UpdateLogoRequest
 * @package App\Http\Requests\Supplier
 */
class UpdateLogoRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'logo' => 'required|image',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}
