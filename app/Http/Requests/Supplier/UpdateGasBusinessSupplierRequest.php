<?php

namespace App\Http\Requests\Supplier;

use App\Models\Provider\Provider;
use App\Models\Supplier\BaseSupplierModel;

/**
 * Class UpdateGasBusinessSupplierRequest
 * @package App\Http\Requests\Supplier
 */
class UpdateGasBusinessSupplierRequest extends UpdateSupplierRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'                     => 'required|string',
            'formula'                  => 'nullable|string',
            'providers'                => 'required|array',
            'providers.*'              => 'required|integer|exists:' . Provider::getTableName() . ',id',
            'features'                 => 'nullable|array',
            'tariffs'                  => 'required|array',
            'tariffs.*.name'           => 'required|string',
            'tariffs.*.price'          => 'required|numeric',
            'tariffs.*.formula'        => 'nullable|string',
            'tariffs.*.condition'      => 'nullable|array',
            'tariffs.*.condition.from' => 'nullable|numeric',
            'tariffs.*.condition.to'   => 'nullable|numeric',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}
