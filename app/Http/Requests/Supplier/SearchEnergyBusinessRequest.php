<?php

namespace App\Http\Requests\Supplier;

use App\Models\Location\Location;

/**
 * Class SearchEnergyBusinessRequest
 * @package App\Http\Requests\Supplier
 */
class SearchEnergyBusinessRequest extends SearchSupplierRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'kWh'      => 'required|numeric',
            'location' => 'required|integer|exists:' . Location::getTableName() . ',id'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}
