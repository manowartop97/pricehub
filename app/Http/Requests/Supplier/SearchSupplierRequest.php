<?php

namespace App\Http\Requests\Supplier;

use App\Http\Requests\Request;
use App\Models\Provider\Provider;
use App\Models\Location\Location;

/**
 * Class SearchSupplierRequest
 * @package App\Http\Requests\Supplier
 */
class SearchSupplierRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'location'     => 'required|integer|exists:' . Location::getTableName() . ',id',
            'gas_provider' => 'nullable|integer|exists:' . Provider::getTableName() . ',id'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}
