<?php

namespace App\Http\Requests\Location;

use App\Http\Requests\Request;

/**
 * Class CreateRequest
 * @package App\Http\Requests\Location
 */
class CreateRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'           => 'required|string',
            'index'          => 'required|integer',
            'osr_price'      => 'required|numeric',
            'osp_price'      => 'required|numeric',
            'exchange_price' => 'required|numeric',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}
