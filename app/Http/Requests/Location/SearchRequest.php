<?php

namespace App\Http\Requests\Location;

use App\Http\Requests\Request;

/**
 * Class SearchRequest
 * @package App\Http\Requests\Location
 */
class SearchRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'index' => 'nullable'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}
