<?php

namespace App\Http\Requests\SupplierRequest;

use App\Http\Requests\Request;

/**
 * Class UploadFileRequest
 * @package App\Http\Requests\SupplierRequest
 */
class UploadFileRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'file' => 'required|file'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}
