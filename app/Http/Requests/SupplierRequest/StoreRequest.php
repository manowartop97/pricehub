<?php

namespace App\Http\Requests\SupplierRequest;

use App\Http\Requests\Request;
use App\Models\SupplierRequest\SupplierRequest;
use App\Services\Supplier\Factory\Factory;

/**
 * Class StoreRequest
 * @package App\Http\Requests\SupplierRequest
 */
class StoreRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        $rules['type'] = 'required|string|in:' . Factory::TYPE_GAS_CLIENT_SUPPLIER . ',' . Factory::TYPE_GAS_BUSINESS_SUPPLIER . ',' . Factory::TYPE_ENERGY_BUSINESS_SUPPLIER;

        if (!is_null($this->get('type', null))) {
            $rules = array_merge(
                $rules,
                collect(SupplierRequest::getSupplierFields($this->get('type'), true))
                    ->mapWithKeys(function ($data) {
                        return [$data['name'] => $data['rule']];
                    })
                    ->toArray()
            );
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}
