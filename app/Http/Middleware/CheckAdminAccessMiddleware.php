<?php

namespace App\Http\Middleware;

use App\Models\User\User;
use Closure;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

/**
 * Class CheckAdminAccessMiddleware
 * @package App\Http\Middleware
 */
class CheckAdminAccessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws Exception
     */
    public function handle(Request $request, Closure $next)
    {
        /** @var User $user */
        if (is_null($user = Auth::user()) || $user->role !== User::ROLE_ADMIN) {
            throw new Exception("You are not allowed to perform this action", Response::HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}
