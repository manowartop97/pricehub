<?php

namespace App\Http\Resources\SupplierRequest;

use App\Models\SupplierRequest\SupplierRequest;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class SupplierRequestResource
 * @package App\Http\Resources\SupplierRequest
 *
 * @mixin SupplierRequest
 */
class SupplierRequestResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request): array
    {
        $fields = [
            'id'   => $this->id,
            'type' => $this->type,
        ];

        return array_merge($fields, $this->data);
    }
}
