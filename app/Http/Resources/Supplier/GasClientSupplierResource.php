<?php

namespace App\Http\Resources\Supplier;

use App\Models\Supplier\GasClientSupplier;
use App\Models\Supplier\SupplierHasProvider;
use App\Models\Supplier\Tariff\Tariff;
use App\Services\Supplier\Factory\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class GasClientSupplierResource
 * @package App\Http\Resources\Supplier
 *
 * @property array $resource
 * @mixin Model|GasClientSupplier
 */
class GasClientSupplierResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        if ($this->resource instanceof Model) {
            return [
                'id'           => $this->id,
                'name'         => $this->name,
                'type'         => Factory::TYPE_GAS_CLIENT_SUPPLIER,
                'logo'         => !is_null($this->logo()) ? asset('storage') . '/' . $this->logo()->path : null,
                'month_result' => number_format($this->result / 12, 2),
                'year_result'  => number_format($this->result, 2),
                'features'     => $this->features,
                'providers'    => $this->supplierProviders
                    ->map(function (SupplierHasProvider $supplierHasProvider) {
                        return [
                            'id'          => $supplierHasProvider->provider_id,
                            'type'        => $supplierHasProvider->provider->type,
                            'name'        => $supplierHasProvider->provider->type,
                            'information' => $supplierHasProvider->provider->information,
                        ];
                    })
                    ->toArray(),
                'tariffs'      => $this->tariffs
                    ->map(function (Tariff $tariff) {
                        return [
                            'id'        => $tariff->id,
                            'name'      => $tariff->name,
                            'price'     => $tariff->price,
                            'condition' => $tariff->condition
                        ];
                    })
                    ->toArray(),
                'created_at'   => optional($this->created_at)->timestamp,
                'updated_at'   => optional($this->updated_at)->timestamp,
            ];
        }

        $price = $this->resource['tariff_price'];
        unset($this->resource['tariff_price']);

        return array_merge(
            $this->resource,
            [
                'month_result' => number_format($price / 12, 2),
                'year_result'  => number_format($price, 2),
            ]
        );
    }
}
