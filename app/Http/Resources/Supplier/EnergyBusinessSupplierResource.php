<?php

namespace App\Http\Resources\Supplier;

use App\Models\Supplier\EnergyBusinessSupplier;
use App\Services\Supplier\Factory\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class EnergyBusinessSupplierResource
 * @package App\Http\Resources\Supplier
 *
 * @mixin EnergyBusinessSupplier
 */
class EnergyBusinessSupplierResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'           => $this->id,
            'name'         => $this->name,
            'type'         => Factory::TYPE_ENERGY_BUSINESS_SUPPLIER,
            'logo'         => !is_null($this->logo()) ? asset('storage') . '/' . $this->logo()->path : null,
            'formula'      => $this->formula,
            'features'     => $this->features,
            'month_result' => number_format($this->result, 2),
            'year_result'  => number_format($this->result * 12, 2),
            'created_at'   => optional($this->created_at)->timestamp,
            'updated_at'   => optional($this->updated_at)->timestamp,
        ];
    }
}
