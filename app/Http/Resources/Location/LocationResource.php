<?php

namespace App\Http\Resources\Location;

use App\Models\Location\Location;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class LocationResource
 * @package App\Http\Resources\Location
 * @mixin Location
 */
class LocationResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'             => $this->id,
            'index'          => $this->index,
            'name'           => $this->name,
            'osr_price'      => $this->osr_price,
            'osp_price'      => $this->osp_price,
            'exchange_price' => $this->exchange_price,
        ];
    }
}
