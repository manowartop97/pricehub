<?php

namespace App\Http\Resources\Provider;

use App\Models\Provider\Provider;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class GasProviderResource
 * @package App\Http\Resources\GasProvider
 *
 * @mixin Provider
 */
class ProviderResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'type'        => $this->type,
            'tariff'      => $this->tariff,
            'tariff_type' => $this->tariff_type,
            'information' => $this->information,
        ];
    }
}
