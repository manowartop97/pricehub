<?php

namespace App\Traits\File;

use App\Models\File\File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * Trait HasFiles
 * @package App\Traits\File
 * @mixin Model
 */
trait HasFiles
{
    /**
     * @return MorphMany
     */
    public function files(): MorphMany
    {
        return $this->morphMany(File::class, 'model');
    }
}
