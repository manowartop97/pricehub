<?php

namespace App\Traits\Collection;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Collection;

/**
 * Trait PaginateCollection
 * @package App\Traits\Collection
 */
trait PaginateCollection
{
    /**
     * @param Collection $collection
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    protected function paginate(Collection $collection, int $perPage = 15): LengthAwarePaginator
    {
        $currentPage = Paginator::resolveCurrentPage();

        $currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);
        parse_str(request()->getQueryString(), $query);
        unset($query['page']);

        return new Paginator(
            $currentPageItems,
            $collection->count(),
            $perPage,
            $currentPage,
            [
                'pageName' => 'page',
                'path'     => Paginator::resolveCurrentPath(),
                'query'    => $query,
            ]
        );
    }
}
