<?php

namespace App\Helpers;

/**
 * Class DataHelper
 * @package App\Helpers
 */
class DataHelper
{
    /**
     * @const
     */
    public const TARIFF_DATA = [
        'Місячний',
        'Річний'
    ];

    /**
     * @const
     */
    public const USAGE_TYPE = [
        'Приготування їжі',
        'Приготування їжі та підігрів води',
        'Комплексно із опаленням'
    ];

    /**
     * @const
     */
    public const APARTMENT_TYPE_DATA = [
        'Приватний будинок',
        'Квартира'
    ];
}
