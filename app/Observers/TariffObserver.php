<?php

namespace App\Observers;

use App\Models\Supplier\Tariff\Tariff;
use App\Services\Supplier\Formula\DefaultGas\DefaultGasFormula;
use Illuminate\Database\Eloquent\Model;

class TariffObserver
{
    /**
     * @param Model|Tariff $model
     * @return void
     */
    public function creating(Model $model): void
    {
        if (is_null($model->formula)) {
            $model->formula = DefaultGasFormula::class;
            return;
        }
    }
}
