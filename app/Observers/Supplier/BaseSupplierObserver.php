<?php

namespace App\Observers\Supplier;

use App\Exceptions\Formula\FormulaClassNotFoundException;
use App\Models\Supplier\BaseSupplierModel;
use App\Services\Supplier\Formula\DefaultFormula\DefaultFormula;
use App\Services\Supplier\Formula\DefaultGas\DefaultGasFormula;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SupplierObserver
 * @package App\Observers
 */
class BaseSupplierObserver
{
    /**
     * @param Model|BaseSupplierModel $model
     * @return void
     */
    public function deleting(Model $model): void
    {
        $model->supplierProviders()->delete();
        $model->tariffs()->delete();
    }
}
