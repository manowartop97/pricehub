<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
      integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://use.fontawesome.com/31efdc989c.js"></script>


<div class="container">
    <div class="content">
        <div class="row mt-3">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            Search form
                        </div>
                        <form action="{{route('search')}}">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Provider To Link</label>
                                        <select name="provider" id="provider" class="form-control">
                                            @foreach($providers as $provider)
                                                <option
                                                    value="{{$provider->getKey()}}" {{$provider->getKey() == \Illuminate\Support\Facades\Request::get('provider') ? 'selected' : ''}}>{{$provider->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Region name</label>
                                        <input type="text" class="form-control region_original" name="region"
                                               value="{{\Illuminate\Support\Facades\Request::get('region')}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">District name</label>
                                        <input type="text" class="form-control district_original" name="district"
                                               value="{{\Illuminate\Support\Facades\Request::get('district')}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">City name</label>
                                        <input type="text" class="form-control city_original" name="city"
                                               value="{{\Illuminate\Support\Facades\Request::get('city')}}">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Street name</label>
                                        <input type="text" class="form-control" name="street"
                                               value="{{\Illuminate\Support\Facades\Request::get('street')}}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <input type="submit" value="Search" class="btn btn-success">
                                </div>
                            </div>
                        </form>
                       <div class="row">
                           <div class="col-md-4">
                               <form action="{{route('link')}}" method="post">
                                   {{csrf_field()}}
                                   <input type="hidden" name="provider"
                                          value="{{\Illuminate\Support\Facades\Request::get('provider', $providers->first()->getKey())}}">
                                   <input type="hidden" name="type" value="1">
                                   <input type="hidden" class="region_input"
                                          name="region"
                                          value="{{\Illuminate\Support\Facades\Request::get('region', null)}}">
                                   <input type="submit" value="Link region" class="btn btn-success">
                               </form>
                           </div>
                           <div class="col-md-4">
                               <form action="{{route('link')}}" method="post">
                                   {{csrf_field()}}
                                   <input type="hidden" name="provider"
                                          value="{{\Illuminate\Support\Facades\Request::get('provider', $providers->first()->getKey())}}">
                                   <input type="hidden" name="type" value="2">
                                   <input type="hidden"
                                          class="district_input"
                                          name="district"
                                          value="{{\Illuminate\Support\Facades\Request::get('district', null)}}">
                                   <input type="submit" value="Link district" class="btn btn-warning">
                               </form>
                           </div>
                           <div class="col-md-4">
                               <form action="{{route('link')}}" method="post">
                                   {{csrf_field()}}
                                   <input type="hidden" name="provider"
                                          value="{{\Illuminate\Support\Facades\Request::get('provider', $providers->first()->getKey())}}">
                                   <input type="hidden" name="type" value="3">
                                   <input type="hidden"
                                          name="city"
                                          class="city_input"
                                          value="{{\Illuminate\Support\Facades\Request::get('city', null)}}">
                                   <input type="submit" value="Link city" class="btn btn-info">
                               </form>
                           </div>
                       </div>
                        <form action="{{route('link')}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="type" value="4">
                            <input type="hidden" name="provider"
                                   value="{{\Illuminate\Support\Facades\Request::get('provider', $providers->first()->getKey())}}">
                            <table class="table">
                                <tr>
                                    <th>Region</th>
                                    <th>District</th>
                                    <th>City</th>
                                    <th>Street</th>
                                    <th>Index</th>
                                    <th></th>
                                </tr>

                                @foreach($models as $model)
                                    <tr class="{{isset($linkedLocations[$model->index]) ? 'linked' : ''}}">
                                        <td>{{$model->street->region_name}}</td>
                                        <td>{{$model->street->district_name}}</td>
                                        <td>{{$model->street->city_name}}</td>
                                        <td>{{$model->street->name}}</td>
                                        <td>{{$model->index}}</td>
                                        <td><input type="checkbox" id="{{$model->index}}" value="{{$model->index}}"
                                                   class="form-control"
                                                   name="indexes[]"></td>
                                    </tr>
                                @endforeach
                            </table>
                            <div class="col-md-12">
                                {{$models->appends(\Illuminate\Support\Facades\Request::all())->links('pagination::bootstrap-4')}}
                            </div>
                            <div class="col-md-12">
                                <input type="submit" value="Link selected locations" class="btn btn-warning">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $(document).on('input', '.region_original', function (e) {
            $('.region_input').val($(this).val());
        });

        $(document).on('input', '.district_original', function (e) {
            $('.district_input').val($(this).val());
        });
        $(document).on('input', '.city_original', function (e) {
            $('.city_input').val($(this).val());
        });
    })
</script>
<style>
    .linked {
        background-color: #ae7070;
    }
</style>


